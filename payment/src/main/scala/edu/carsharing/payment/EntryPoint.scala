package edu.carsharing.payment

import cats.effect.{ ExitCode, IO, IOApp }
import io.chrisdavenport.log4cats.Logger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import org.http4s.server.blaze.BlazeServerBuilder

object EntryPoint extends IOApp {
  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  def run(args: List[String]) =
    for {
      _ <- BlazeServerBuilder[IO](executionContext)
            .bindHttp(8090, "0.0.0.0")
            .withHttpApp(new PaymentServer[IO].app)
            .serve
            .compile
            .drain
    } yield ExitCode.Success
}
