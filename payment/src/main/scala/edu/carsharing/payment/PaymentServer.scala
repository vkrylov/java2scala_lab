package edu.carsharing.payment

import java.util.UUID

import cats.effect.{ Concurrent, Timer }
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.{ Defer, Monad }
import edu.carsharing.payment.PaymentServer.Payment
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.{ HttpRoutes, _ }
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.middleware._

final class PaymentServer[F[_]: Monad: Defer: Concurrent: Timer] extends Http4sDsl[F] {

  private val loggers: HttpApp[F] => HttpApp[F] = {
    { http: HttpApp[F] =>
      RequestLogger.httpApp(true, true)(http)
    } andThen { http: HttpApp[F] =>
      ResponseLogger.httpApp(true, true)(http)
    }
  }

  implicit val decoder = jsonOf[F, Payment]

  val ar @ httpRoute = HttpRoutes.of[F] {
    case req @ POST -> Root / "payments" =>
      for {
        _ <- req.as[Payment]
        resp <- Ok(UUID.randomUUID().toString.asJson)
      } yield (resp)
  }

  val app: HttpApp[F] = loggers(httpRoute.orNotFound)
}

object PaymentServer {
  case class Card(name: String, number: Long, expiration: String, ccv: Int)
  case class Payment(id: UUID, total: Double, card: Card)
}
