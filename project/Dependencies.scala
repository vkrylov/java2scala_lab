import sbt._

object Dependencies {

  object Versions {
    val tapir         = "0.17.7"
    val sttpClient    = "2.2.8"
    val cats          = "2.3.1"
    val catsRetry     = "2.1.0"
    val catsMtl       = "1.1.1"
    val log4cats      = "1.1.1"
    val circe         = "0.13.0"
    val http4s        = "0.21.15"
    val newtype       = "0.4.4"
    val squants       = "1.7.0"
    val http4sJwtAuth = "0.0.5"
    val skunk         = "0.0.22"
    val refined       = "0.9.20"
    val ciris         = "1.2.1"
    val redis4cats    = "0.11.1"

    val slf4j   = "1.7.30"
    val logback = "1.2.3"

    val betterMonadicFor = "0.3.1"
    val kindProjector    = "0.11.3"

    val scalaCheck    = "1.15.2"
    val scalaTest     = "3.2.2"
    val scalaTestPlus = "3.2.3.0"
  }

  object Libraries {
    def http4sModule(artifact: String): ModuleID = "org.http4s" %% artifact % Versions.http4s

    val http4s =
      Seq(
        http4sModule("http4s-dsl"),
        http4sModule("http4s-blaze-server"),
        http4sModule("http4s-blaze-client"),
        http4sModule("http4s-circe")
      )

    val sttpClient = Seq("com.softwaremill.sttp.client" %% "core" % Versions.sttpClient)

    val cats = Seq(
      "org.typelevel"     %% "cats-effect"    % Versions.cats,
      "com.github.cb372"  %% "cats-retry"     % Versions.catsRetry,
      "org.typelevel"     %% "cats-mtl"       % Versions.catsMtl,
      "io.chrisdavenport" %% "log4cats-slf4j" % Versions.log4cats
    )

    val refined = Seq("eu.timepit" %% "refined" % Versions.refined, "eu.timepit" %% "refined-cats" % Versions.refined)

    def isCir(artifact: String): ModuleID = "is.cir" %% artifact % Versions.ciris

    val ciris = Seq(isCir("ciris"), isCir("ciris-enumeratum"), isCir("ciris-refined"))

    val skunk = Seq(
      "org.tpolecat" %% "skunk-core"  % Versions.skunk,
      "org.tpolecat" %% "skunk-circe" % Versions.skunk
    )

    val redis = Seq(
      "dev.profunktor" %% "redis4cats-effects"  % Versions.redis4cats,
      "dev.profunktor" %% "redis4cats-log4cats" % Versions.redis4cats
    )

    val log = Seq(
      "org.slf4j" % "slf4j-nop"            % Versions.slf4j,
      "ch.qos.logback" % "logback-classic" % Versions.logback
    )

    val other = Seq(
      "io.estatico"    %% "newtype"         % Versions.newtype,
      "org.typelevel"  %% "squants"         % Versions.squants,
      "dev.profunktor" %% "http4s-jwt-auth" % Versions.http4sJwtAuth
    )

    val betterMonadicFor = "com.olegpy"    %% "better-monadic-for" % Versions.betterMonadicFor
    val kindProjector    = "org.typelevel" %  "kind-projector"     % Versions.kindProjector

    val circe = Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser",
      "io.circe" %% "circe-refined"
    ).map(_ % Versions.circe)

    val test = Seq(
      "org.scalacheck"    %% "scalacheck"      % Versions.scalaCheck,
      "org.scalatest"     %% "scalatest"       % Versions.scalaTest,
      "org.scalatestplus" %% "scalacheck-1-15" % Versions.scalaTestPlus
    ).map(_ % Test)
  }

}
