#!/bin/zsh

## Admin
export ADMIN_JWT_TOKEN="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1dWlkIjogIjZlYWQ1YmFjLWE1YTAtNDUyNy1iNzkwLTI1OTQwZjFmYjkzYiJ9.MhHOWUQHlXcU9-n9kZKItuo2FfhOzRKCjYtpURYJd6c"
export ADMIN_AUTH_HEADER="Authorization: Bearer ${ADMIN_JWT_TOKEN}"

## Auth
export JWT_TOKEN="<user_token>"
export AUTH_HEADER="Authorization: Bearer ${JWT_TOKEN}"
# Create user
curl -i -X POST -d '{"username": "user1", "password": "password1"}' http://localhost:8080/v1/auth/users
# Login
curl -i -X POST -d '{"username": "user1", "password": "password1"}' http://localhost:8080/v1/auth/login
# Logout
curl -i -X POST -H "${AUTH_HEADER}" http://localhost:8080/v1/auth/logout

## Health check
curl -i -X GET http://localhost:8080/v1/healthcheck

## Car models
# Admin
curl -i -X POST -H "${ADMIN_AUTH_HEADER}" -H "Content-Type: application/json" -H "Content-Length: 9" -d '"vw polo"' http://localhost:8080/v1/admin/models
curl -i -X DELETE -H "${ADMIN_AUTH_HEADER}" -H "Content-Type: application/json" -H "Content-Length: 9" -d '"vw polo"' http://localhost:8080/v1/admin/models
# All
curl -i -X GET http://localhost:8080/v1/models

## Tariffs
# Admin
curl -i -X POST -H "${ADMIN_AUTH_HEADER}" -H "Content-Type: application/json" -H "Content-Length: 40" -d '{"name":"light","hold":2.5,"active":7.5}' http://localhost:8080/v1/admin/tariffs

## Cars
# Admin
curl -i -X POST -H "${ADMIN_AUTH_HEADER}" -H "Content-Type: application/json" -d '{"licensePlate":"E707KX26RUS","model":"vw polo"}' http://localhost:8080/v1/admin/cars
# All
curl -i -X GET http://localhost:8080/v1/cars

## Rents
# Secured
curl -i -X POST -H "${AUTH_HEADER}" -H "Content-Type: application/json" -d '{"tariffName":"light","licensePlate":"E707KX26RUS"}' http://localhost:8080/v1/rent/create
curl -i -X POST -H "${AUTH_HEADER}" -H "Content-Type: application/json" -d '"ActivateParam"' http://localhost:8080/v1/rent/change
curl -i -X POST -H "${AUTH_HEADER}" -H "Content-Type: application/json" -d '"OnHoldParam"' http://localhost:8080/v1/rent/change

## Checkout
# Secured
curl -i -X POST -H "${AUTH_HEADER}" -H "Content-Type: application/json" -H "Content-Length: 79" -d '{"name":"Name Surname","number":1234567890123456,"expiration":"2002","ccv":123}' http://localhost:8080/v1/checkout

## Payment client
curl -i -X POST -H "Content-Type: application/json" -d '{"id":"6ead5bac-a5a0-4527-b790-25940f1fb93b","total":12.3,"card":{"name":"Name Surname","number":1234567890123456,"expiration":"2002","ccv":123}}' http://localhost:8090/payments
