# Carsharing
Application from people for people

## Docker
Build images:
```bash
sbt docker:publishLocal
sbt payment/docker:publishLocal
```
Run images:
```bash
docker-compose -f ./app/docker-compose.yml up
```

Run all services:
```bash
docker network create carsharing
docker-compose up
docker-compose -f ./app/docker-compose.yml up
```
