package edu.carsharing

import cats.effect.{IO, Resource}
import cats.instances.int._
import cats.instances.string._
import ciris.Secret
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.carmodel.LiveCarModels
import edu.carsharing.algebras.cars.LiveCars
import edu.carsharing.algebras.crypto.LiveCrypto
import edu.carsharing.algebras.orders.LiveOrders
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.algebras.tariffs.LiveTariffs
import edu.carsharing.algebras.users.LiveUsers
import edu.carsharing.config.data.PasswordSalt
import edu.carsharing.domain.carmodel._
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.rents._
import edu.carsharing.domain.tariffs.TariffName
import edu.carsharing.suite.ResourceSuite
import edu.carsharing.suite.generators._
import edu.carsharing.suite.utils.IOAssertion
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.ops._
import natchez.Trace.Implicits.noop
import skunk.Session
import squants.Money

class PostgreSQLTest extends ResourceSuite[Resource[IO, Session[IO]]] {
  // For it:tests, one test is enough
  val MaxTests: PropertyCheckConfigParam = MinSuccessful(1)

  override def resources =
    Session.pooled[IO](host = "localhost", port = 5432, user = "postgres", database = "carsharing", max = 10)

  test("Car models") {
    withResources { pool =>
      forAll(MaxTests) { (carModel: CarModel) =>
        IOAssertion {
          LiveCarModels.make[IO](pool).flatMap { b =>
            for {
              x <- b.findAll()
              _ <- b.create(carModel.name)
              y <- b.findAll()
              z <- b.create(carModel.name).attempt
            } yield assert(
              x.isEmpty &&
                y.count(_.name == carModel.name) === 1 &&
                z.isLeft
            )
          }
        }
      }
    }
  }

  lazy val salt = Secret("53kr3t": NonEmptyString).coerce[PasswordSalt]

  test("Users") {
    withResources { pool =>
      forAll(MaxTests) { (username: UserName, password: Password) =>
        IOAssertion {
          for {
            c <- LiveCrypto.make[IO](salt)
            u <- LiveUsers.make[IO](pool, c)
            d <- u.create(username, password)
            x <- u.find(username, password)
            y <- u.find(username, Password("foo"))
            z <- u.create(username, password).attempt
          } yield assert(
            x.count(_.id == d) === 1 &&
              y.isEmpty && z.isLeft
          )
        }
      }
    }
  }

  test("Orders") {
    withResources { pool =>
      forAll(MaxTests) {
        (
          ids: (OrderId, PaymentId, RentId),
          crs: (ModelName, LicensePlate),
          tps: (TariffName, Money, Money),
          unp: (UserName, Password),
          items: List[RentEvent],
          price: Money
        ) =>
          val (oid, pid, rid) = ids
          val (mn, lp)        = crs
          val (tn, h, a)      = tps
          val (un, pw)        = unp
          IOAssertion {
            for {
              cms <- LiveCarModels.make[IO](pool)
              cs <- LiveCars.make[IO](pool)
              ts <- LiveTariffs.make[IO](pool)
              o <- LiveOrders.make[IO](pool)
              c <- LiveCrypto.make[IO](salt)
              u <- LiveUsers.make[IO](pool, c)
              _ <- cms.create(mn)
              _ <- cs.create(lp, mn)
              _ <- ts.create(tn, h, a)
              d <- u.create(un, pw)
              x <- o.findBy(d)
              y <- o.get(d, oid)
              i <- o.create(d, pid, lp, tn, rid, items, price)
            } yield assert(
              x.isEmpty && y.isEmpty &&
                i.uuid.version === 4 // UUID version
            )
          }
      }
    }
  }
}
