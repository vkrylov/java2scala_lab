CREATE TABLE users (
    uuid UUID PRIMARY KEY,
    name VARCHAR UNIQUE NOT NULL,
    password VARCHAR NOT NULL
);

CREATE TABLE car_models (
    uuid UUID PRIMARY KEY,
    name VARCHAR UNIQUE NOT NULL
);

CREATE TABLE cars (
    uuid UUID PRIMARY KEY,
    license_plate VARCHAR UNIQUE NOT NULL,
    car_model_id UUID NOT NULL,
    CONSTRAINT car_model_id_fkey FOREIGN KEY (car_model_id)
    REFERENCES car_models (uuid) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE tariffs (
    uuid UUID PRIMARY KEY,
    name VARCHAR UNIQUE NOT NULL,
    hold NUMERIC,
    active NUMERIC
);

CREATE TABLE orders (
    uuid UUID PRIMARY KEY,
    user_id UUID NOT NULL,
    car_id UUID NOT NULL,
    tariff_id UUID NOT NULL,
    rent_id UUID NOT NULL,
    payment_id UUID UNIQUE NOT NULL,
    events JSONB NOT NULL,
    total NUMERIC,
    CONSTRAINT user_id_fkey FOREIGN KEY (user_id)
    REFERENCES users (uuid) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT car_id_fkey FOREIGN KEY (car_id)
    REFERENCES cars (uuid) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT tariff_id_fkey FOREIGN KEY (tariff_id)
    REFERENCES tariffs (uuid) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);
