package edu.carsharing

import cats.effect._
import dev.profunktor.auth.jwt._
import io.circe.Decoder
import io.circe.parser.{ decode => jsonDecode }
import io.estatico.newtype.macros._
import pdi.jwt._
import scala.io.StdIn.readLine

object TokenGenerator extends IOApp {
  @newtype case class Claim(value: String)

  object Claim {
    implicit val jsonDecoder: Decoder[Claim] =
      Decoder.forProduct1("claim")(Claim.apply)
  }

  private val printF = (arg: Any) => IO(println(arg))

  def run(args: List[String]): IO[ExitCode] = {
    val algo = JwtAlgorithm.HS256

    for {
      _ <- printF("""Type claim (can be any valid claim json like {"claim": "sample-claim"}):""")
      claimStr <- IO(readLine())
      c <- IO(JwtClaim(claimStr))
      _ <- printF(c.content)
      _ <- printF("Type secret key")
      secretKeyStr <- IO(readLine())
      sk = JwtSecretKey(secretKeyStr)
      t <- jwtEncode[IO](c, sk, algo)
      _ <- printF(t)
      jwtAuth = JwtAuth.hmac(sk.value, algo)
      decoded <- jwtDecode[IO](t, jwtAuth)
      dt <- IO.fromEither(jsonDecode[Claim](decoded.content))
      _ <- printF(dt)
    } yield ExitCode.Success
  }

}
