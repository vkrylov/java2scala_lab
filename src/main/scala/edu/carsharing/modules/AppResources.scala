package edu.carsharing.modules

import scala.concurrent.ExecutionContext

import cats.effect.{ ConcurrentEffect, ContextShift, Resource }
import dev.profunktor.redis4cats.{ Redis, RedisCommands }
import dev.profunktor.redis4cats.connection.{ RedisClient, RedisURI }
import edu.carsharing.config.data._
import io.chrisdavenport.log4cats.Logger
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import skunk.{ Session, SessionPool }
import cats.syntax.apply._
import dev.profunktor.redis4cats.data.RedisCodec
import dev.profunktor.redis4cats.log4cats._
import natchez.Trace.Implicits.noop

final case class AppResources[F[_]](
  client: Client[F],
  psql: Resource[F, Session[F]],
  redis: RedisCommands[F, String, String]
)

object AppResources {

  def make[F[_]: ConcurrentEffect: ContextShift: Logger](cfg: AppConfig): Resource[F, AppResources[F]] = {
    def mkPostgreSqlResource(c: PostgreSQLConfig): SessionPool[F] =
      Session.pooled[F](
        host = c.host.value,
        port = c.port.value,
        user = c.user.value,
        database = c.database.value,
        max = c.max.value
      )

    def mkRedisResource(c: RedisConfig): Resource[F, RedisCommands[F, String, String]] =
      for {
        uri <- Resource.liftF(RedisURI.make[F](c.uri.value.value))
        client <- RedisClient[F].fromUri(uri)
        cmd <- Redis[F].fromClient(client, RedisCodec.Utf8)
      } yield cmd

    def mkHttpClient(c: HttpClientConfig): Resource[F, Client[F]] =
      BlazeClientBuilder[F](ExecutionContext.global)
        .withConnectTimeout(c.connectTimeout)
        .withRequestTimeout(c.requestTimeout)
        .resource

    (mkHttpClient(cfg.httpClientConfig), mkPostgreSqlResource(cfg.postgreSQL), mkRedisResource(cfg.redis))
      .mapN(AppResources.apply[F])
  }

}
