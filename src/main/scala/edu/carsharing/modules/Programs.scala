package edu.carsharing.modules

import cats.effect.{ Sync, Timer }
import edu.carsharing.config.data.CheckoutConfig
import io.chrisdavenport.log4cats.Logger
import retry.RetryPolicies.limitRetries
import retry.RetryPolicies.exponentialBackoff
import cats.syntax.semigroup._
import edu.carsharing.effects.Background
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.programs.CheckoutProgram
import retry.RetryPolicy

object Programs {
  def make[F[_]: Background: Logger: Sync: Timer](
    checkoutConfig: CheckoutConfig,
    algebras: Algebras[F],
    clients: HttpClients[F]
  ): F[Programs[F]] =
    Sync[F].delay(new Programs[F](checkoutConfig, algebras, clients))
}

final class Programs[F[_]: Background: Logger: MonadThrow: Timer] private (
  cfg: CheckoutConfig,
  algebras: Algebras[F],
  clients: HttpClients[F]
) {
  val retryPolicy: RetryPolicy[F] =
    limitRetries[F](cfg.retriesLimit.value) |+| exponentialBackoff[F](cfg.retriesBackoff)

  def checkout: CheckoutProgram[F] =
    new CheckoutProgram[F](clients.payment, algebras.rentService, algebras.orders, retryPolicy)
}
