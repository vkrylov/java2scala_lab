package edu.carsharing.modules

import cats.effect.{ Resource, Sync }
import cats.syntax.flatMap._
import cats.syntax.functor._
import dev.profunktor.auth.jwt._
import dev.profunktor.redis4cats.RedisCommands
import edu.carsharing.algebras.tokens.LiveTokens
import edu.carsharing.algebras.auth._
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.{ AdminUser, CommonUser, User }
import edu.carsharing.algebras.crypto.LiveCrypto
import edu.carsharing.algebras.users.LiveUsers
import edu.carsharing.effects.effects.ApThrow
import edu.carsharing.config.data.AppConfig
import io.circe.parser.{ decode => jsonDecode }
import pdi.jwt.JwtAlgorithm
import skunk.Session

object Security {
  def make[F[_]: Sync](
    cfg: AppConfig,
    sessionPool: Resource[F, Session[F]],
    redis: RedisCommands[F, String, String]
  ): F[Security[F]] = {
    val adminJwtAuth: AdminJwtAuth =
      AdminJwtAuth(JwtAuth.hmac(cfg.adminJwtConfig.secretKey.value.value.value, JwtAlgorithm.HS256))

    val userJwtAuth: UserJwtAuth =
      UserJwtAuth(JwtAuth.hmac(cfg.tokenConfig.value.value.value, JwtAlgorithm.HS256))

    val adminToken = JwtToken(cfg.adminJwtConfig.adminToken.value.value.value)

    for {
      adminClaim <- jwtDecode[F](adminToken, adminJwtAuth.value)
      content <- ApThrow[F].fromEither(jsonDecode[ClaimContent](adminClaim.content))
      adminUser = AdminUser(User(UserId(content.uuid), UserName("admin")))
      tokens <- LiveTokens.make[F](cfg.tokenConfig, cfg.tokenExpiration)
      crypto <- LiveCrypto.make[F](cfg.passwordSalt)
      users <- LiveUsers.make[F](sessionPool, crypto)
      auth <- LiveAuth.make[F](cfg.tokenExpiration, tokens, users, redis)
      adminAuth <- LiveAdminAuth.make[F](adminToken, adminUser)
      usersAuth <- LiveUsersAuth.make[F](redis)
    } yield new Security[F](auth, adminAuth, usersAuth, adminJwtAuth, userJwtAuth)
  }
}

final class Security[F[_]] private (
  val auth: Auth[F],
  val adminAuth: UsersAuth[F, AdminUser],
  val usersAuth: UsersAuth[F, CommonUser],
  val adminJwtAuth: AdminJwtAuth,
  val userJwtAuth: UserJwtAuth
)
