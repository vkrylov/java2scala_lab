package edu.carsharing.modules

import cats.Parallel
import cats.effect.{ Concurrent, Resource, Timer }
import cats.syntax.flatMap._
import cats.syntax.functor._
import dev.profunktor.redis4cats.RedisCommands
import edu.carsharing.algebras.carmodel.{ CarModels, LiveCarModels }
import edu.carsharing.algebras.cars.{ Cars, LiveCars }
import edu.carsharing.algebras.healthcheck.{ HealthCheck, LiveHealthCheck }
import edu.carsharing.algebras.orders.{ LiveOrders, Orders }
import edu.carsharing.algebras.rents.{ LiveRents, Rents }
import edu.carsharing.algebras.tariffs.{ LiveTariffs, Tariffs }
import edu.carsharing.config.data.RentExpiration
import skunk.Session

object Algebras {
  def make[F[_]: Concurrent: Parallel: Timer](
    redis: RedisCommands[F, String, String],
    sessionPool: Resource[F, Session[F]],
    rentExpiration: RentExpiration
  ): F[Algebras[F]] =
    for {
      tariffs <- LiveTariffs.make[F](sessionPool)
      carModels <- LiveCarModels.make[F](sessionPool)
      cars <- LiveCars.make[F](sessionPool)
      rentService <- LiveRents.make[F](tariffs, redis, rentExpiration)
      orders <- LiveOrders.make[F](sessionPool)
      health <- LiveHealthCheck.make[F](sessionPool, redis)
    } yield new Algebras[F](tariffs, carModels, cars, rentService, orders, health)
}

final class Algebras[F[_]] private (
  val tariffs: Tariffs[F],
  val carModels: CarModels[F],
  val cars: Cars[F],
  val rentService: Rents[F],
  val orders: Orders[F],
  val healthCheck: HealthCheck[F]
)
