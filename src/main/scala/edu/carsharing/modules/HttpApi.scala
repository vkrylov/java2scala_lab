package edu.carsharing.modules

import cats.effect.{ Concurrent, Sync, Timer }
import dev.profunktor.auth.JwtAuthMiddleware
import dev.profunktor.auth.jwt.JwtToken
import org.http4s.server.Router
import org.http4s.{ HttpApp, HttpRoutes }
import org.http4s.server.middleware._
import pdi.jwt.JwtClaim
import cats.syntax.semigroupk._
import scala.concurrent.duration._

import edu.carsharing.algebras.auth.UsersAuth._
import edu.carsharing.http.routes._
import edu.carsharing.http.routes.admin.{ AdminCarModelRoutes, AdminCarRoutes, AdminTariffRoutes }
import edu.carsharing.http.routes.auth._
import edu.carsharing.http.routes.secured.{ CheckoutRoutes, RentRoutes }
import org.http4s.implicits._

object HttpApi {
  def make[F[_]: Concurrent: Timer](
    algebras: Algebras[F],
    programs: Programs[F],
    security: Security[F]
  ): F[HttpApi[F]] =
    Sync[F].delay(new HttpApi[F](algebras, programs, security))
}

final class HttpApi[F[_]: Concurrent: Timer] private (
  algebras: Algebras[F],
  programs: Programs[F],
  security: Security[F]
) {
  private val adminAuth: JwtToken => JwtClaim => F[Option[AdminUser]] =
    t => c => security.adminAuth.findUser(t)(c)

  private val usersAuth: JwtToken => JwtClaim => F[Option[CommonUser]] =
    t => c => security.usersAuth.findUser(t)(c)

  private val adminMiddleware =
    JwtAuthMiddleware[F, AdminUser](security.adminJwtAuth.value, adminAuth)

  private val usersMiddleware =
    JwtAuthMiddleware[F, CommonUser](security.userJwtAuth.value, usersAuth)

  // Auth routes
  private val loginRoutes =
    new LoginRoutes[F](security.auth).routes
  private val logoutRoutes =
    new LogoutRoutes[F](security.auth).routes(usersMiddleware)
  private val userRoutes =
    new UserRoutes[F](security.auth).routes

  // Open routes
  private val healthRoutes =
    new HealthRoutes[F](algebras.healthCheck).routes
  private val carModelRoutes =
    new CarModelRoutes[F](algebras.carModels).routes
  private val carsRoutes =
    new CarRoutes[F](algebras.cars).routes

  // Secured routes
  private val rentRoutes =
    new RentRoutes[F](algebras.rentService).routes(usersMiddleware)
  private val checkoutRoutes =
    new CheckoutRoutes[F](programs.checkout).routes(usersMiddleware)
  // TODO create and add Tariff and Order routes

  // Admin routes
  private val adminCarModelRoutes =
    new AdminCarModelRoutes[F](algebras.carModels).routes(adminMiddleware)
  private val adminCarRoutes =
    new AdminCarRoutes[F](algebras.cars).routes(adminMiddleware)
  private val adminTariffRoutes =
    new AdminTariffRoutes[F](algebras.tariffs).routes(adminMiddleware)

  // Combining all the http routes
  private val openRoutes: HttpRoutes[F] =
    healthRoutes <+> carModelRoutes <+> carsRoutes <+> loginRoutes <+> userRoutes <+>
        logoutRoutes <+> rentRoutes <+> checkoutRoutes
  private val adminRoutes: HttpRoutes[F] =
    adminCarModelRoutes <+> adminCarRoutes <+> adminTariffRoutes

  private val routes: HttpRoutes[F] = Router(version.v1 -> openRoutes, version.v1 + "/admin" -> adminRoutes)

  private val middleware: HttpRoutes[F] => HttpRoutes[F] = {
    { http: HttpRoutes[F] =>
      AutoSlash(http)
    } andThen { http: HttpRoutes[F] =>
      CORS(http, CORS.DefaultCORSConfig)
    } andThen { http: HttpRoutes[F] =>
      Timeout(60.seconds)(http)
    }
  }

  private val loggers: HttpApp[F] => HttpApp[F] = {
    { http: HttpApp[F] =>
      RequestLogger.httpApp(true, true)(http)
    } andThen { http: HttpApp[F] =>
      ResponseLogger.httpApp(true, true)(http)
    }
  }

  val httpApp: HttpApp[F] = loggers(middleware(routes).orNotFound)

}
