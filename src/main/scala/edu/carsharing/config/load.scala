package edu.carsharing.config

import scala.concurrent.duration._

import cats.effect._
import cats.instances.string._
import cats.syntax.parallel._
import ciris._
import ciris.refined._
import edu.carsharing.algebras.tokens.Tokens._
import data._
import environments.AppEnvironment
import environments.AppEnvironment._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.string.NonEmptyString

object load {

  def apply[F[_]: Async: ContextShift]: F[AppConfig] =
    env("CS_APP_ENV")
      .as[AppEnvironment]
      .flatMap {
        case Container =>
          default(
            redisUri = RedisURI("redis://redis"),
            paymentUri = PaymentURI("http://payment:8090"),
            postgresUri = "postgres"
          )
        case Local =>
          default(
            redisUri = RedisURI("redis://localhost"),
            paymentUri = PaymentURI("http://localhost:8090"),
            postgresUri = "localhost"
          )
      }
      .load[F]

  private def default(redisUri: RedisURI, paymentUri: PaymentURI, postgresUri: NonEmptyString): ConfigValue[AppConfig] =
    (
      env("CS_JWT_SECRET_KEY").as[NonEmptyString].secret,
      env("CS_JWT_CLAIM").as[NonEmptyString].secret,
      env("CS_ACCESS_TOKEN_SECRET_KEY").as[NonEmptyString].secret,
      env("CS_ADMIN_USER_TOKEN").as[NonEmptyString].secret,
      env("CS_PASSWORD_SALT").as[NonEmptyString].secret
    ).parMapN { (secretKey, claimStr, tokenKey, adminToken, salt) =>
      AppConfig(
        AdminJwtConfig(JwtSecretKeyConfig(secretKey), JwtClaimConfig(claimStr), AdminUserTokenConfig(adminToken)),
        JwtSecretKeyConfig(tokenKey),
        PasswordSalt(salt),
        TokenExpiration(30.minutes),
        RentExpiration(30.minutes),
        CheckoutConfig(retriesLimit = 3, retriesBackoff = 10.milliseconds),
        PaymentConfig(paymentUri),
        HttpClientConfig(connectTimeout = 2.seconds, requestTimeout = 2.seconds),
        PostgreSQLConfig(host = postgresUri, port = 5432, user = "postgres", database = "carsharing", max = 10),
        RedisConfig(redisUri),
        HttpServerConfig(host = "0.0.0.0", port = 8080)
      )
    }
}
