package edu.carsharing

import cats.effect.{ ExitCode, IO, IOApp }
import edu.carsharing.modules._
import io.chrisdavenport.log4cats.Logger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import org.http4s.server.blaze.BlazeServerBuilder

object Main extends IOApp {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def run(args: List[String]): IO[ExitCode] =
    config.load[IO].flatMap { cfg =>
      Logger[IO].info(s"Loaded config $cfg") *>
        AppResources.make[IO](cfg).use { res =>
          for {
            sec <- Security.make[IO](cfg, res.psql, res.redis)
            alg <- Algebras.make[IO](res.redis, res.psql, cfg.rentExpiration)
            cli <- HttpClients.make[IO](cfg.paymentConfig, res.client)
            pro <- Programs.make[IO](cfg.checkoutConfig, alg, cli)
            api <- HttpApi.make[IO](alg, pro, sec)
            _ <- BlazeServerBuilder[IO](executionContext)
                  .bindHttp(cfg.httpServerConfig.port.value, cfg.httpServerConfig.host.value)
                  .withHttpApp(api.httpApp)
                  .serve
                  .compile
                  .drain
          } yield ExitCode.Success
        }
    }

}
