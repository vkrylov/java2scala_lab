package edu.carsharing.programs

import scala.concurrent.duration._

import cats.effect.Timer
import cats.syntax.applicativeError._
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.monadError._
import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.algebras.rents.Rents
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.checkout.Card
import edu.carsharing.domain.rents._
import edu.carsharing.domain.tariffs.TariffName
import edu.carsharing.effects.Background
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.client.PaymentClient
import edu.carsharing.http.client.PaymentClient.Payment
import io.chrisdavenport.log4cats.Logger
import retry.RetryDetails.{ GivingUp, WillDelayAndRetry }
import retry.{ retryingOnAllErrors, RetryDetails, RetryPolicy }
import squants.Money

final class CheckoutProgram[F[_]: Background: MonadThrow: Logger: Timer](
  paymentClient: PaymentClient[F],
  rentService: Rents[F],
  orders: Orders[F],
  retryPolicy: RetryPolicy[F]
) {
  def checkout(userId: UserId, card: Card): F[OrderId] =
    rentService
      .total(userId)
      .ensure(EmptyRentError)(_.events.nonEmpty)
      .flatMap {
        case RentSummary(events, total, licensePlate, rentId, tariffName) =>
          for {
            pid <- processPayment(Payment(userId, total, card))
            order <- createOrder(userId, licensePlate, rentId, tariffName, pid, events, total)
            _ <- rentService.delete(userId).attempt.void
          } yield order
      }

  private def createOrder(
    userId: UserId,
    licensePlate: LicensePlate,
    rentId: RentId,
    tariffName: TariffName,
    paymentId: PaymentId,
    events: List[RentEvent],
    total: Money
  ): F[OrderId] = {
    val action = retryingOnAllErrors[OrderId](policy = retryPolicy, onError = logError("Order"))(
      orders.create(userId, paymentId, licensePlate: LicensePlate, tariffName, rentId, events, total)
    )

    def bgAction(fa: F[OrderId]): F[OrderId] =
      fa.adaptError {
          case e => OrderError(e.getMessage)
        }
        .onError {
          case _ =>
            Logger[F]
              .error(s"Failed to create order for Payment: $paymentId. Rescheduling as a background action") *>
                Background[F].schedule(bgAction(fa), 1.hour)
        }

    bgAction(action)
  }

  private def processPayment(payment: Payment): F[PaymentId] = {
    val action = retryingOnAllErrors[PaymentId](policy = retryPolicy, onError = logError("Payments"))(
      paymentClient.process(payment)
    )

    action.adaptError {
      case e =>
        PaymentError(Option(e.getMessage).getOrElse("Unknown"))
    }
  }

  private def logError(action: String)(e: Throwable, details: RetryDetails): F[Unit] =
    details match {
      case r: WillDelayAndRetry =>
        Logger[F].error(
          s"Failed to process $action with ${e.getMessage}. So far we have retried ${r.retriesSoFar} times."
        )
      case g: GivingUp =>
        Logger[F].error(s"Giving up on $action after ${g.totalRetries} retries.")
    }
}
