package edu.carsharing.algebras.tariffs

import edu.carsharing.common.skunkx._
import edu.carsharing.domain.tariffs._
import skunk._
import skunk.codec.numeric._
import skunk.codec.text._
import skunk.codec.uuid._
import skunk.implicits._
import squants.Money
import squants.market.RUB

object TariffQueries {
  val encoder: Encoder[Tariff] =
    (uuid.cimap[TariffId] ~ varchar.cimap[TariffName] ~ numeric.contramap[Money](_.amount) ~ numeric.contramap[Money](
          _.amount
        )).contramap { t =>
      t.tariffId ~ t.name ~ t.hold ~ t.active
    }

  val insertTariff: Command[Tariff] =
    sql"""
         INSERT INTO tariffs VALUES($encoder);
         """.command

  val decoder: Decoder[Tariff] =
    (uuid.cimap[TariffId] ~ varchar.cimap[TariffName] ~ numeric.map(RUB.apply) ~ numeric.map(RUB.apply)).map {
      case (id ~ name ~ hold ~ active) => Tariff(id, name, hold, active)
    }

  val selectByName: Query[TariffName, Tariff] =
    sql"""
         SELECT uuid, name, hold, active FROM tariffs WHERE name = ${varchar.cimap[TariffName]};
         """.query(decoder)
}
