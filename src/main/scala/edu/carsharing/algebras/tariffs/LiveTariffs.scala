package edu.carsharing.algebras.tariffs

import cats.effect.{ Resource, Sync }
import edu.carsharing.algebras.tariffs.TariffQueries._
import edu.carsharing.domain.tariffs._
import edu.carsharing.effects.GenUUID
import edu.carsharing.effects.effects.BracketThrow
import skunk._
import squants.Money
import cats.syntax.functor._
import cats.syntax.flatMap._

final class LiveTariffs[F[_]: BracketThrow: GenUUID](sessionPool: Resource[F, Session[F]]) extends Tariffs[F] {
  override def create(name: TariffName, hold: Money, active: Money): F[Unit] =
    sessionPool.use { session =>
      session.prepare(insertTariff).use { cmd =>
        GenUUID[F].make[TariffId].flatMap { id =>
          cmd.execute(Tariff(id, name, hold, active)).void
        }
      }
    }

  override def findByName(name: TariffName): F[Option[Tariff]] =
    sessionPool.use { session =>
      session.prepare(selectByName).use { _.option(name) }
    }
}

object LiveTariffs {
  def make[F[_]: Sync](sessionPool: Resource[F, Session[F]]): F[LiveTariffs[F]] =
    Sync[F].delay {
      new LiveTariffs[F](sessionPool)
    }
}
