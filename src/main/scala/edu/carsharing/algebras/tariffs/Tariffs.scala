package edu.carsharing.algebras.tariffs

import edu.carsharing.domain.tariffs._
import squants.Money

trait Tariffs[F[_]] {
  def create(name: TariffName, hold: Money, active: Money): F[Unit]
  def findByName(name: TariffName): F[Option[Tariff]]
}
