package edu.carsharing.algebras.rents

import java.util.UUID
import java.util.concurrent.TimeUnit

import scala.concurrent.duration._
import scala.util.control.NoStackTrace

import cats.effect.{ Clock, Sync }
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.functor._
import dev.profunktor.redis4cats.RedisCommands
import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.rents.LiveRents._
import edu.carsharing.algebras.tariffs.Tariffs
import edu.carsharing.config.data.RentExpiration
import edu.carsharing.domain.cars._
import edu.carsharing.domain.rents._
import edu.carsharing.domain.tariffs._
import edu.carsharing.effects.GenUUID
import edu.carsharing.effects.effects._
import io.estatico.newtype.ops._
import squants.Money
import squants.market.RUB

final class LiveRents[F[_]: GenUUID: MonadThrow: Clock] private (
  tariffs: Tariffs[F],
  redis: RedisCommands[F, String, String],
  exp: RentExpiration
) extends Rents[F] {

  override def create(userId: UserId, tariffName: TariffName, licensePlate: LicensePlate): F[Unit] =
    GenUUID[F].make[RentId].flatMap { rentId =>
      for {
        _ <- redis.hmSet(
              userId.value.toString,
              Map(TariffKey -> tariffName.name, RentKey -> rentId.uuid, LicencePlateKey -> licensePlate.number).view
                .mapValues(_.toString)
                .toMap
            )
        _ <- onHold(userId)
      } yield ()
    } *> redis.expire(userId.value.toString, exp.duration).map(_ => ())

  override def onHold(userId: UserId): F[Unit] = addEvent(userId, HoldEvent)

  override def activate(userId: UserId): F[Unit] = addEvent(userId, ActiveEvent)

  override def total(userId: UserId): F[RentSummary] =
    redis.hGetAll(userId.value.toString).flatMap { kvs =>
      if (kvs.nonEmpty) {
        for {
          s <- realTimeSeconds
          total <- calcTotal(kvs + (s.toString -> CompleteEvent))
        } yield total
      } else {
        MonadThrow[F].raiseError(NotStarted)
      }
    }

  override def delete(userId: UserId): F[Unit] = redis.del(userId.value.toString).map(_ => ())

  private def calcTotal(kvs: Map[String, String]): F[RentSummary] = {
    case class Entry(
      tariffName: Option[TariffName],
      rentId: Option[RentId],
      licensePlate: Option[LicensePlate],
      events: Vector[RentEvent]
    )
    val entry = kvs.foldLeft(Entry(None, None, None, Vector.empty)) {
      case (e, (k, v)) =>
        k match {
          case `TariffKey`       => e.copy(tariffName = Some(TariffName(v)))
          case `RentKey`         => e.copy(rentId = Some(UUID.fromString(v).coerce[RentId]))
          case `LicencePlateKey` => e.copy(licensePlate = Some(LicensePlate(v)))
          case _                 => e.copy(events = e.events :+ RentEvent(k.toLong.seconds, v))
        }
    }

    val preparedEntry = entry.copy(events = entry.events.sortBy(_.timestamp))

    val entryF = MonadThrow[F].fromOption({
      import preparedEntry._
      for {
        t <- tariffName
        r <- rentId
        p <- licensePlate
      } yield (t, r, p, events)
    }, new Exception("Cannot parse rent") with NoStackTrace)

    for {
      (tn, rid, lp, events) <- entryF
      tOpt <- tariffs.findByName(tn)
      tariff <- ApThrow[F].fromOption(tOpt, new Exception(s"Tariff with name $tn not found") with NoStackTrace)
      total = calcTotal(events, tariff)
    } yield RentSummary(events.toList, total, lp, rid, tn)
  }

  private def calcTotal(events: Vector[RentEvent], tariff: Tariff): Money = {
    val (total, _) = events.foldLeft((RUB(0), Option.empty[RentEvent])) {
      case ((t, None), e) => (t, Some(e))
      case ((t, last @ Some(RentEvent(ts, et))), e) =>
        if (et != e.eventType) {
          val price = if (et == HoldEvent) tariff.hold else tariff.active
          val sum   = t + (price * (e.timestamp.toSeconds - ts.toSeconds).toDouble)
          (sum, Some(e))
        } else {
          (t, last)
        }
    }

    total
  }

  private def addEvent(userId: UserId, eventType: String): F[Unit] =
    for {
      s <- realTimeSeconds
      _ <- redis.hSet(userId.value.toString, s.toString, eventType)
    } yield ()

  private def realTimeSeconds: F[Long] = Clock[F].realTime(TimeUnit.SECONDS)
}

object LiveRents {
  val TariffKey       = "tariff"
  val LicencePlateKey = "license_plate"
  val RentKey         = "rent"

  def make[F[_]: Sync: MonadThrow: Clock](
    tariffs: Tariffs[F],
    redis: RedisCommands[F, String, String],
    exp: RentExpiration
  ): F[Rents[F]] =
    Sync[F].delay {
      new LiveRents[F](tariffs, redis, exp)
    }
}
