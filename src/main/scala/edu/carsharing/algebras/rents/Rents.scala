package edu.carsharing.algebras.rents

import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.rents.RentSummary
import edu.carsharing.domain.tariffs.TariffName

trait Rents[F[_]] {
  def create(userId: UserId, tariffName: TariffName, licensePlate: LicensePlate): F[Unit]
  def onHold(userId: UserId): F[Unit]
  def activate(userId: UserId): F[Unit]
  def total(userId: UserId): F[RentSummary]
  def delete(userId: UserId): F[Unit]
}
