package edu.carsharing.algebras.crypto

import javax.crypto.Cipher

import edu.carsharing.algebras.auth.Auth.Password
import edu.carsharing.algebras.crypto.Crypto.EncryptedPassword
import io.estatico.newtype.macros.newtype

trait Crypto {
  def encrypt(value: Password): EncryptedPassword
  def decrypt(value: EncryptedPassword): Password
}

object Crypto {
  @newtype case class EncryptedPassword(value: String)

  @newtype case class EncryptCipher(value: Cipher)
  @newtype case class DecryptCipher(value: Cipher)
}
