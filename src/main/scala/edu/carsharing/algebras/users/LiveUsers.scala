package edu.carsharing.algebras.users

import cats.effect.{ Resource, Sync }
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.option._
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.User
import edu.carsharing.algebras.crypto.Crypto
import edu.carsharing.effects.GenUUID
import edu.carsharing.effects.effects.BracketThrow
import skunk._
import skunk.implicits._

final class LiveUsers[F[_]: BracketThrow: GenUUID] private (sessionPool: Resource[F, Session[F]], crypto: Crypto)
    extends Users[F] {
  import UserQueries._

  def find(username: UserName, password: Password): F[Option[User]] =
    sessionPool.use { session =>
      session.prepare(selectUser).use { q =>
        q.option(username).map {
          case Some(u ~ p) if p.value == crypto.encrypt(password).value =>
            u.some
          case _ =>
            none[User]
        }
      }
    }

  def create(username: UserName, password: Password): F[UserId] =
    sessionPool.use { session =>
      session.prepare(insertUser).use { cmd =>
        GenUUID[F].make[UserId].flatMap { id =>
          cmd
            .execute(User(id, username) ~ crypto.encrypt(password))
            .as(id)
            .handleErrorWith {
              case SqlState.UniqueViolation(_) =>
                UserNameInUse(username).raiseError[F, UserId]
            }
        }
      }
    }

}

object LiveUsers {
  def make[F[_]: Sync](sessionPool: Resource[F, Session[F]], crypto: Crypto): F[Users[F]] =
    Sync[F].delay(new LiveUsers[F](sessionPool, crypto))
}
