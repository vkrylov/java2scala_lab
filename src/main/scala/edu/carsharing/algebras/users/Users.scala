package edu.carsharing.algebras.users

import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.User

trait Users[F[_]] {
  def find(username: UserName, password: Password): F[Option[User]]
  def create(username: UserName, password: Password): F[UserId]
}
