package edu.carsharing.algebras.users

import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.User
import edu.carsharing.algebras.crypto.Crypto.EncryptedPassword
import edu.carsharing.common.skunkx._
import skunk._
import skunk.codec.text._
import skunk.codec.uuid._
import skunk.implicits._

private object UserQueries {

  val codec: Codec[User ~ EncryptedPassword] =
    (uuid.cimap[UserId] ~ varchar.cimap[UserName] ~
        varchar.cimap[EncryptedPassword]).imap {
      case i ~ n ~ p => User(i, n) ~ p
    } {
      case u ~ p =>
        u.id ~ u.name ~ p
    }

  val selectUser: Query[UserName, User ~ EncryptedPassword] = sql"""
        SELECT * FROM users
        WHERE name = ${varchar.cimap[UserName]} """.query(codec)

  val insertUser: Command[User ~ EncryptedPassword] = sql"""
        INSERT INTO users VALUES ($codec) """.command

}
