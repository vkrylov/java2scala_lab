package edu.carsharing.algebras.auth

import cats.Applicative
import cats.effect.Sync
import cats.instances.option._
import cats.syntax.alternative._
import cats.syntax.applicative._
import cats.syntax.functor._
import dev.profunktor.auth.jwt.JwtToken
import edu.carsharing.algebras.auth.UsersAuth.AdminUser
import pdi.jwt.JwtClaim

class LiveAdminAuth[F[_]: Applicative](adminToken: JwtToken, adminUser: AdminUser) extends UsersAuth[F, AdminUser] {
  def findUser(token: JwtToken)(claim: JwtClaim): F[Option[AdminUser]] =
    (token == adminToken).guard[Option].as(adminUser).pure[F]

}

object LiveAdminAuth {
  def make[F[_]: Sync](adminToken: JwtToken, adminUser: AdminUser): F[UsersAuth[F, AdminUser]] =
    Sync[F].delay(new LiveAdminAuth(adminToken, adminUser))
}
