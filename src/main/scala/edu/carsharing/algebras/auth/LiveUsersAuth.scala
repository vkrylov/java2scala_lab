package edu.carsharing.algebras.auth

import cats.Functor
import cats.effect.Sync
import cats.syntax.functor._
import dev.profunktor.auth.jwt.JwtToken
import dev.profunktor.redis4cats.RedisCommands
import edu.carsharing.algebras.auth.UsersAuth.{ CommonUser, User }
import edu.carsharing.http.json.codecs._
import io.circe.parser._
import pdi.jwt.JwtClaim

class LiveUsersAuth[F[_]: Functor](redis: RedisCommands[F, String, String]) extends UsersAuth[F, CommonUser] {
  override def findUser(token: JwtToken)(claim: JwtClaim): F[Option[CommonUser]] =
    redis
      .get(token.value)
      .map(_.flatMap(u => decode[User](u).toOption.map(CommonUser.apply)))
}

object LiveUsersAuth {
  def make[F[_]: Sync](redis: RedisCommands[F, String, String]): F[UsersAuth[F, CommonUser]] =
    Sync[F].delay(new LiveUsersAuth(redis))
}
