package edu.carsharing.algebras.auth

import cats.effect.Sync
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.apply._
import cats.syntax.flatMap._
import cats.syntax.functor._
import dev.profunktor.auth.jwt.JwtToken
import dev.profunktor.redis4cats.RedisCommands
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.User
import edu.carsharing.algebras.tokens.Tokens
import edu.carsharing.algebras.tokens.Tokens.TokenExpiration
import edu.carsharing.algebras.users.Users
import edu.carsharing.effects.GenUUID
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.json.codecs._
import io.circe.syntax._

final class LiveAuth[F[_]: GenUUID: MonadThrow] private (
  tokenExpiration: TokenExpiration,
  tokens: Tokens[F],
  users: Users[F],
  redis: RedisCommands[F, String, String]
) extends Auth[F] {
  private val TokenExpiration = tokenExpiration.value

  def newUser(username: UserName, password: Password): F[JwtToken] =
    users.find(username, password).flatMap {
      case Some(_) => UserNameInUse(username).raiseError[F, JwtToken]
      case None =>
        for {
          i <- users.create(username, password)
          t <- tokens.create
          u = User(i, username).asJson.noSpaces
          _ <- redis.setEx(t.value, u, TokenExpiration)
          _ <- redis.setEx(username.value, t.value, TokenExpiration)
        } yield t
    }

  def login(username: UserName, password: Password): F[JwtToken] =
    users.find(username, password).flatMap {
      case None => InvalidUserOrPassword(username).raiseError[F, JwtToken]
      case Some(user) =>
        redis.get(username.value).flatMap {
          case Some(t) => JwtToken(t).pure[F]
          case None =>
            tokens.create.flatTap { t =>
              redis.setEx(t.value, user.asJson.noSpaces, TokenExpiration) *>
                redis.setEx(username.value, t.value, TokenExpiration)
            }
        }
    }

  def logout(token: JwtToken, username: UserName): F[Unit] =
    redis.del(token.value) *> redis.del(username.value).map(_ => ())

}

object LiveAuth {
  def make[F[_]: Sync](
    tokenExpiration: TokenExpiration,
    tokens: Tokens[F],
    users: Users[F],
    redis: RedisCommands[F, String, String]
  ): F[Auth[F]] =
    Sync[F].delay(new LiveAuth(tokenExpiration, tokens, users, redis))
}
