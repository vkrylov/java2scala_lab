package edu.carsharing.algebras.auth

import dev.profunktor.auth.jwt.JwtToken
import edu.carsharing.algebras.auth.Auth._
import io.estatico.newtype.macros.newtype
import pdi.jwt.JwtClaim

trait UsersAuth[F[_], A] {
  def findUser(token: JwtToken)(claim: JwtClaim): F[Option[A]]
}

object UsersAuth {
  case class User(id: UserId, name: UserName)
  @newtype case class CommonUser(value: User)
  @newtype case class AdminUser(value: User)
}
