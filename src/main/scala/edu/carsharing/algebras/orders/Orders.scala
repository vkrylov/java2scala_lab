package edu.carsharing.algebras.orders

import java.util.UUID

import scala.util.control.NoStackTrace

import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.rents._
import edu.carsharing.domain.tariffs._
import io.estatico.newtype.macros.newtype
import squants.Money

trait Orders[F[_]] {
  def get(userId: UserId, orderId: OrderId): F[Option[Order]]

  def findBy(userId: UserId): F[List[Order]]

  def create(
    userId: UserId,
    paymentId: PaymentId,
    licensePlate: LicensePlate,
    tariffName: TariffName,
    rentId: RentId,
    events: List[RentEvent],
    total: Money
  ): F[OrderId]
}

object Orders {
  @newtype case class OrderId(uuid: UUID)
  @newtype case class PaymentId(uuid: UUID)

  case class Order(
    id: OrderId,
    pid: PaymentId,
    licensePlate: LicensePlate,
    tariffName: TariffName,
    rentId: RentId,
    events: List[RentEvent],
    total: Money
  )

  case object EmptyRentError extends NoStackTrace
  case class OrderError(cause: String) extends NoStackTrace
  case class PaymentError(cause: String) extends NoStackTrace
}
