package edu.carsharing.algebras.orders

import cats.effect.{ Resource, Sync }
import cats.syntax.flatMap._
import cats.syntax.functor._
import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.rents.{ RentEvent, RentId }
import edu.carsharing.domain.tariffs.TariffName
import edu.carsharing.effects.GenUUID
import skunk.Session
import skunk.implicits._
import squants.Money

private class LiveOrders[F[_]: Sync](sessionPool: Resource[F, Session[F]]) extends Orders[F] {

  import OrderQueries._

  def get(userId: UserId, orderId: OrderId): F[Option[Order]] =
    sessionPool.use { session =>
      session.prepare(selectByUserIdAndOrderId).use { q =>
        q.option(userId ~ orderId)
      }
    }

  def findBy(userId: UserId): F[List[Order]] = sessionPool.use { session =>
    session.prepare(selectByUserId).use { q =>
      q.stream(userId, 1024).compile.toList
    }
  }

  def create(
    userId: UserId,
    paymentId: PaymentId,
    licensePlate: LicensePlate,
    tariffName: TariffName,
    rentId: RentId,
    events: List[RentEvent],
    total: Money
  ): F[OrderId] =
    sessionPool.use { session =>
      session.prepare(insertOrder).use { cmd =>
        GenUUID[F].make[OrderId].flatMap { id =>
          val order = Order(id, paymentId, licensePlate, tariffName, rentId, events, total)
          cmd.execute(userId ~ order).as(id)
        }
      }
    }

}

object LiveOrders {

  def make[F[_]: Sync](sessionPool: Resource[F, Session[F]]): F[Orders[F]] =
    Sync[F].delay(new LiveOrders[F](sessionPool))

}
