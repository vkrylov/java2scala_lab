package edu.carsharing.algebras.orders

import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.rents.{ RentEvent, RentId }
import edu.carsharing.domain.tariffs.TariffName
import skunk.circe.codec.all._
import skunk.codec.numeric._
import skunk.codec.uuid._
import skunk.codec.text._
import skunk.implicits._
import skunk._
import squants.Money
import squants.market.RUB
import edu.carsharing.http.json.codecs._

private object OrderQueries {
  import edu.carsharing.common.skunkx._

  val decoder: Decoder[Order] = (
    uuid.cimap[OrderId]
      ~ varchar.cimap[LicensePlate]
      ~ varchar.cimap[TariffName]
      ~ uuid.cimap[RentId]
      ~ uuid.cimap[PaymentId]
      ~ jsonb[List[RentEvent]]
      ~ numeric.map(RUB.apply)
  ).map {
    case o ~ l ~ t ~ r ~ p ~ es ~ tt => Order(o, p, l, t, r, es, tt)
  }

  val selectByUserId: Query[UserId, Order] =
    sql"""
      SELECT o.uuid, c.license_plate, t.name, o.rent_id, o.payment_id, o.events, o.total
      FROM orders AS o
      INNER JOIN cars AS c ON c.uuid = o.car_id
      INNER JOIN tariffs AS t ON t.uuid = o.tariff_id
      WHERE o.user_id = ${uuid.cimap[UserId]};
      """.query(decoder)

  val selectByUserIdAndOrderId: Query[UserId ~ OrderId, Order] =
    sql"""
      SELECT o.uuid, c.license_plate, t.name, o.rent_id, o.payment_id, o.events, o.total
      FROM orders AS o
      INNER JOIN cars AS c ON c.uuid = o.car_id
      INNER JOIN tariffs AS t ON t.uuid = o.tariff_id
      WHERE o.user_id = ${uuid.cimap[UserId]} AND o.uuid = ${uuid.cimap[OrderId]};
      """.query(decoder)

  val insertOrder: Command[UserId ~ Order] =
    sql"""
      INSERT INTO orders(
        uuid,
        user_id,
        car_id,
        tariff_id,
        rent_id,
        payment_id,
        events, total)
      SELECT
        ${uuid.cimap[OrderId]},
        ${uuid.cimap[UserId]},
        c.uuid,
        t.uuid,
        ${uuid.cimap[RentId]},
        ${uuid.cimap[PaymentId]},
        ${jsonb[List[RentEvent]]},
        ${numeric.contramap[Money](_.amount)}
      FROM cars AS c, tariffs AS t
      WHERE c.license_plate = ${varchar.cimap[LicensePlate]} and t.name = ${varchar.cimap[TariffName]};
      """.command.contramap {
      case uid ~ order =>
        order.id ~ uid ~ order.rentId ~ order.pid ~ order.events ~ order.total ~ order.licensePlate ~ order.tariffName
    }

}
