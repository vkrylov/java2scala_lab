package edu.carsharing.algebras.tokens

import scala.concurrent.duration.FiniteDuration

import ciris.Secret
import dev.profunktor.auth.jwt.JwtToken
import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.macros.newtype

trait Tokens[F[_]] {
  def create: F[JwtToken]
}

object Tokens {
  @newtype case class AdminUserTokenConfig(value: Secret[NonEmptyString])
  @newtype case class JwtSecretKeyConfig(value: Secret[NonEmptyString])
  @newtype case class JwtClaimConfig(value: Secret[NonEmptyString])
  @newtype case class TokenExpiration(value: FiniteDuration)
}
