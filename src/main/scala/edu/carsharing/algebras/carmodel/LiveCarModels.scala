package edu.carsharing.algebras.carmodel

import cats.effect.{ Resource, Sync }
import cats.syntax.flatMap._
import cats.syntax.functor._
import edu.carsharing.domain.carmodel._
import edu.carsharing.effects.GenUUID
import edu.carsharing.effects.effects.BracketThrow
import skunk.Session

final class LiveCarModels[F[_]: BracketThrow: GenUUID: Sync] private (sessionPool: Resource[F, Session[F]])
    extends CarModels[F] {
  import CarModelQueries._

  override def findAll(): F[List[CarModel]] = sessionPool.use(_.execute(selectAll))

  override def create(name: ModelName): F[Unit] = sessionPool.use { session =>
    session.prepare(insertCarModel).use { cmd =>
      GenUUID[F].make[ModelId].flatMap { id =>
        cmd.execute(CarModel(id, name)).void
      }
    }
  }

  override def findBy(name: ModelName): F[List[CarModel]] =
    sessionPool.use { session =>
      session.prepare(selectByModelName).use { q =>
        q.stream(name, 1024).compile.toList
      }
    }

  override def delete(name: ModelName): F[Unit] =
    sessionPool.use { session =>
      session.prepare(deleteByModelName).use {
        _.execute(name).void
      }
    }
}

object LiveCarModels {
  def make[F[_]: Sync](sessionPool: Resource[F, Session[F]]): F[CarModels[F]] =
    Sync[F].delay(new LiveCarModels[F](sessionPool))
}
