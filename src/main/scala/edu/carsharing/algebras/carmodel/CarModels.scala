package edu.carsharing.algebras.carmodel

import edu.carsharing.domain.carmodel.{ CarModel, ModelName }

trait CarModels[F[_]] {
  def findAll(): F[List[CarModel]]
  def findBy(name: ModelName): F[List[CarModel]]
  def create(name: ModelName): F[Unit]
  def delete(name: ModelName): F[Unit]
}
