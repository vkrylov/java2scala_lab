package edu.carsharing.algebras.carmodel

import edu.carsharing.common.skunkx._
import edu.carsharing.domain.carmodel._
import skunk._
import skunk.codec.text._
import skunk.codec.uuid._
import skunk.implicits._

private object CarModelQueries {

  val codec: Codec[CarModel] =
    (uuid.cimap[ModelId] ~ varchar.cimap[ModelName]).imap {
      case i ~ n => CarModel(i, n)
    }(m => m.id ~ m.name)

  val codec2: Codec[CarModel] = (uuid ~ varchar).imap {
    case i ~ n =>
      CarModel(ModelId(i), ModelName(n))
  }(m => m.id.id -> m.name.name)

  val selectAll: Query[Void, CarModel] =
    sql"""
          SELECT * FROM car_models;
          """.query(codec)

  val insertCarModel: Command[CarModel] =
    sql"""
          INSERT INTO car_models VALUES ($codec);
          """.command

  val selectByModelName: Query[ModelName, CarModel] =
    sql"""
          SELECT * FROM car_models WHERE name = ${varchar.cimap[ModelName]};
          """.query(codec)

  val deleteByModelName: Command[ModelName] =
    sql"""
          DELETE FROM car_models WHERE name = ${varchar.cimap[ModelName]};
          """.command
}
