package edu.carsharing.algebras.healthcheck

import edu.carsharing.algebras.healthcheck.HealthCheck.AppStatus
import io.estatico.newtype.macros.newtype

trait HealthCheck[F[_]] {
  def status: F[AppStatus]
}

object HealthCheck {
  @newtype case class RedisStatus(value: Boolean)
  @newtype case class PostgresStatus(value: Boolean)

  case class AppStatus(redis: RedisStatus, postgres: PostgresStatus)
}
