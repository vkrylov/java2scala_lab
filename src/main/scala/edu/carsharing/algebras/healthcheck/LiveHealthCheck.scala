package edu.carsharing.algebras.healthcheck

import cats.Parallel
import cats.effect.{ Concurrent, Resource, Sync, Timer }
import dev.profunktor.redis4cats.RedisCommands
import skunk.{ Query, Session }
import skunk.implicits._
import skunk.Void
import skunk.codec.numeric._
import cats.effect.syntax.concurrent._
import cats.syntax.functor._
import cats.syntax.applicativeError._
import cats.syntax.parallel._
import cats.syntax.applicative._
import scala.concurrent.duration._

import edu.carsharing.algebras.healthcheck.HealthCheck._

final class LiveHealthCheck[F[_]: Concurrent: Parallel: Timer] private (
  sessionPool: Resource[F, Session[F]],
  redis: RedisCommands[F, String, String]
) extends HealthCheck[F] {

  val q: Query[Void, Int] = sql"SELECT pid FROM pg_stat_activity".query(int4)

  val redisHealth: F[RedisStatus] =
    redis.ping
      .map(_.nonEmpty)
      .timeout(1.second)
      .orElse(false.pure[F])
      .map(RedisStatus.apply)

  val postgresHealth: F[PostgresStatus] =
    sessionPool
      .use(_.execute(q))
      .map(_.nonEmpty)
      .timeout(1.second)
      .orElse(false.pure[F])
      .map(PostgresStatus.apply)

  val status: F[AppStatus] =
    (redisHealth, postgresHealth).parMapN(AppStatus)

}

object LiveHealthCheck {
  def make[F[_]: Concurrent: Parallel: Timer](
    sessionPool: Resource[F, Session[F]],
    redis: RedisCommands[F, String, String]
  ): F[HealthCheck[F]] =
    Sync[F].delay(new LiveHealthCheck[F](sessionPool, redis))
}
