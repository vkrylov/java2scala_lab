package edu.carsharing.algebras.cars
import scala.util.control.NoStackTrace

import cats.Monad
import cats.effect.{ Resource, Sync }
import cats.syntax.flatMap._
import edu.carsharing.algebras.cars.CarQueries._
import edu.carsharing.domain.carmodel.ModelName
import edu.carsharing.domain.cars._
import edu.carsharing.effects.GenUUID
import edu.carsharing.effects.effects.{ ApThrow, BracketThrow }
import skunk.Session
import skunk.data.Completion.Insert

final class LiveCars[F[_]: BracketThrow: GenUUID](sessionPool: Resource[F, Session[F]]) extends Cars[F] {
  override def create(licensePlate: LicensePlate, modelName: ModelName): F[Unit] =
    sessionPool.use { session =>
      session.prepare(insertCar).use { cmd =>
        GenUUID[F].make[CarId].flatMap { id =>
          cmd.execute(Car(id, licensePlate, modelName)).flatMap {
            case Insert(count) =>
              if (count == 1) Monad[F].unit
              else ApThrow[F].raiseError(new Exception(s"Wrong car insert count: $count") with NoStackTrace)
            case completion =>
              ApThrow[F].raiseError(new Exception(s"Wrong car insert completion: $completion") with NoStackTrace)
          }
        }
      }
    }

  override def findAll(): F[List[Car]] =
    sessionPool.use(_.execute(selectAll))
}

object LiveCars {
  def make[F[_]: Sync](sessionPool: Resource[F, Session[F]]): F[Cars[F]] =
    Sync[F].delay(new LiveCars[F](sessionPool))
}
