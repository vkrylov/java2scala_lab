package edu.carsharing.algebras.cars

import edu.carsharing.common.skunkx._
import edu.carsharing.domain.carmodel.ModelName
import edu.carsharing.domain.cars._
import skunk._
import skunk.codec.text._
import skunk.codec.uuid._
import skunk.implicits._

object CarQueries {
  val decoder: Decoder[Car] = (uuid.cimap[CarId] ~ varchar.cimap[LicensePlate] ~ varchar.cimap[ModelName]).map {
    case c ~ lp ~ mn => Car(c, lp, mn)
  }

  val selectAll: Query[Void, Car] =
    sql"""
         SELECT c.uuid, c.license_plate, cm.name FROM cars AS c
         INNER JOIN car_models AS cm
         ON cm.uuid = c.car_model_id;
         """.query(decoder)

  val insertCar: Command[Car] =
    sql"""
         INSERT INTO cars(uuid, license_plate, car_model_id)
           SELECT $uuid, $varchar, uuid FROM car_models WHERE name = $varchar;
         """.command.contramap {
      case c: Car => c.id.uuid ~ c.licensePlate.number ~ c.modelName.name
    }

}
