package edu.carsharing.algebras.cars

import edu.carsharing.domain.carmodel.ModelName
import edu.carsharing.domain.cars._

trait Cars[F[_]] {
  def create(licensePlate: LicensePlate, modelName: ModelName): F[Unit]
  def findAll(): F[List[Car]]
}
