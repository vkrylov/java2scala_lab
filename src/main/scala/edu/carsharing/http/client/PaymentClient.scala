package edu.carsharing.http.client

import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.http.client.PaymentClient.Payment
import edu.carsharing.domain.checkout.Card
import squants.Money

trait PaymentClient[F[_]] {
  def process(payment: Payment): F[PaymentId]
}

object PaymentClient {
  case class Payment(id: UserId, total: Money, card: Card)
}
