package edu.carsharing.http.client

import cats.syntax.applicativeError._
import cats.syntax.either._
import cats.syntax.flatMap._
import cats.syntax.functor._
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.config.data.PaymentConfig
import edu.carsharing.effects.effects.{ BracketThrow, MonadThrow }
import edu.carsharing.http.client.PaymentClient.Payment
import edu.carsharing.http.json.codecs._
import org.http4s.Method.POST
import org.http4s.circe.{ JsonDecoder, _ }
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.{ Status, Uri }

class LivePaymentClient[F[_]: JsonDecoder: MonadThrow: BracketThrow](cfg: PaymentConfig, client: Client[F])
    extends PaymentClient[F]
    with Http4sClientDsl[F] {
  def process(payment: Payment): F[PaymentId] =
    for {
      uri <- Uri.fromString(cfg.uri.value.value + "/payments").liftTo[F]
      req <- POST(payment, uri)
      res <- client.run(req).use { r =>
              if (r.status == Status.Ok || r.status == Status.Conflict) {
                r.asJsonDecode[PaymentId]
              } else {
                PaymentError(Option(r.status.reason).getOrElse("Unknown")).raiseError[F, PaymentId]
              }
            }
    } yield res
}
