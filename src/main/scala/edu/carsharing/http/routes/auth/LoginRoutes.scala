package edu.carsharing.http.routes.auth

import cats.Defer
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import edu.carsharing.algebras.auth.Auth
import edu.carsharing.algebras.auth.Auth.{ InvalidUserOrPassword, LoginUser }
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.decoder._
import edu.carsharing.http.json.codecs._
import org.http4s.HttpRoutes
import org.http4s.circe.JsonDecoder
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

final class LoginRoutes[F[_]: Defer: MonadThrow: JsonDecoder](auth: Auth[F]) extends Http4sDsl[F] {
  private[routes] val prefixPath = "/auth"

  private val httpPath: HttpRoutes[F] = HttpRoutes.of {
    case req @ POST -> Root / "login" =>
      req.decodeR[LoginUser] { user =>
        auth
          .login(user.username.toDomain, user.password.toDomain)
          .flatMap(Ok(_))
          .recoverWith {
            case InvalidUserOrPassword(_) => Forbidden()
          }
      }
  }

  val routes: HttpRoutes[F] = Router(prefixPath -> httpPath)

}
