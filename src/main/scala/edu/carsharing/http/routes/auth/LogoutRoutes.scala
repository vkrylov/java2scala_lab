package edu.carsharing.http.routes.auth

import cats.instances.option._
import cats.syntax.apply._
import cats.syntax.foldable._
import cats.{ Defer, Monad }
import dev.profunktor.auth.AuthHeaders
import edu.carsharing.algebras.auth.Auth
import edu.carsharing.algebras.auth.UsersAuth.CommonUser
import org.http4s.dsl.Http4sDsl
import org.http4s.server.{ AuthMiddleware, Router }
import org.http4s.{ AuthedRoutes, HttpRoutes }

final class LogoutRoutes[F[_]: Defer: Monad](auth: Auth[F]) extends Http4sDsl[F] {

  private[routes] val pathPrefix = "/auth"
  private val httpRoutes: AuthedRoutes[CommonUser, F] = AuthedRoutes.of {
    case ar @ POST -> Root / "logout" as user =>
      AuthHeaders
        .getBearerToken(ar.req)
        .traverse_(t => auth.logout(t, user.value.name)) *> NoContent()
  }

  def routes(authMiddleware: AuthMiddleware[F, CommonUser]): HttpRoutes[F] =
    Router(pathPrefix -> authMiddleware(httpRoutes))

}
