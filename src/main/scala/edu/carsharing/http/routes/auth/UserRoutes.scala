package edu.carsharing.http.routes.auth

import cats.Defer
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import edu.carsharing.algebras.auth.Auth
import edu.carsharing.algebras.auth.Auth.{ CreateUser, UserNameInUse }
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.decoder._
import edu.carsharing.http.json.codecs._
import org.http4s.HttpRoutes
import org.http4s.circe.JsonDecoder
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

final class UserRoutes[F[_]: Defer: JsonDecoder: MonadThrow](auth: Auth[F]) extends Http4sDsl[F] {
  private[routes] val prefixPath = "/auth"

  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of {
    case req @ POST -> Root / "users" =>
      req.decodeR[CreateUser] { user =>
        auth
          .newUser(user.username.toDomain, user.password.toDomain)
          .flatMap(Created(_))
          .recoverWith {
            case UserNameInUse(u) => Conflict(u.value)
          }
      }
  }

  val routes: HttpRoutes[F] = Router(prefixPath -> httpRoutes)
}
