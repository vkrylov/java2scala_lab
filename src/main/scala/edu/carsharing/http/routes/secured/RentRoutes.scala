package edu.carsharing.http.routes.secured

import cats.syntax.flatMap._
import cats.{ Defer, Monad }
import edu.carsharing.algebras.auth.UsersAuth.CommonUser
import edu.carsharing.algebras.rents.Rents
import edu.carsharing.domain.rents.RentTypeChangeParam._
import edu.carsharing.domain.rents._
import edu.carsharing.http.json.codecs._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.{ AuthMiddleware, Router }
import org.http4s.{ AuthedRoutes, HttpRoutes }

final class RentRoutes[F[_]: Defer: JsonDecoder: Monad](rentService: Rents[F]) extends Http4sDsl[F] {

  private[routes] val prefixPath = "/rent"

  private val changeHttpRoutes: AuthedRoutes[CommonUser, F] =
    AuthedRoutes.of {
      case ar @ POST -> Root as user =>
        ar.req
          .asJsonDecode[RentTypeChangeParam]
          .flatMap { param =>
            Ok(param match {
              case ActivateParam => rentService.activate(user.value.id)
              case OnHoldParam   => rentService.onHold(user.value.id)
            })
          }
    }

  private val createHttpRoutes: AuthedRoutes[CommonUser, F] =
    AuthedRoutes.of {
      case ar @ POST -> Root as user =>
        ar.req
          .asJsonDecode[CreateRentParams]
          .flatMap { params =>
            val (tariffName, licensePlate) = params.toDomain
            Created(rentService.create(user.value.id, tariffName, licensePlate))
          }
    }

  private val totalHttpRoutes: AuthedRoutes[CommonUser, F] =
    AuthedRoutes.of {
      case GET -> Root as user =>
        Ok(rentService.total(user.value.id))
    }

  def routes(authMiddleware: AuthMiddleware[F, CommonUser]): HttpRoutes[F] =
    Router(
      s"$prefixPath/create" -> authMiddleware(createHttpRoutes),
      s"$prefixPath/change" -> authMiddleware(changeHttpRoutes),
      s"$prefixPath" -> authMiddleware(totalHttpRoutes)
    )
}
