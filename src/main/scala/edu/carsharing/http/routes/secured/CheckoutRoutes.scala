package edu.carsharing.http.routes.secured

import cats.Defer
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import edu.carsharing.algebras.auth.UsersAuth.CommonUser
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.http.decoder._
import edu.carsharing.domain.checkout.Card
import edu.carsharing.domain.rents.RentNotFound
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.json.codecs._
import edu.carsharing.programs.CheckoutProgram
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.{ AuthMiddleware, Router }

final class CheckoutRoutes[F[_]: Defer: JsonDecoder: MonadThrow](program: CheckoutProgram[F]) extends Http4sDsl[F] {

  private[routes] val prefixPath = "/checkout"

  private val httpRoutes: AuthedRoutes[CommonUser, F] =
    AuthedRoutes.of {
      case ar @ POST -> Root as user =>
        ar.req
          .decodeR[Card] { card =>
            program
              .checkout(user.value.id, card)
              .flatMap(Created(_))
          }
          .recoverWith {
            case RentNotFound(userId) =>
              NotFound(s"Cart not found for user: ${userId.value}")
            case EmptyRentError =>
              BadRequest("Shopping cart is empty!")
            case PaymentError(cause) =>
              BadRequest(cause)
            case OrderError(cause) =>
              BadRequest(cause)
          }
    }

  def routes(authMiddleware: AuthMiddleware[F, CommonUser]): HttpRoutes[F] =
    Router(prefixPath -> authMiddleware(httpRoutes))

}
