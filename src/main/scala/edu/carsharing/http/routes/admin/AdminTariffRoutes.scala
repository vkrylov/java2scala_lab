package edu.carsharing.http.routes.admin

import cats.Defer
import edu.carsharing.algebras.auth.UsersAuth.AdminUser
import edu.carsharing.algebras.tariffs.Tariffs
import edu.carsharing.domain.tariffs.TariffCreateParam
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.decoder._
import edu.carsharing.http.json.codecs._
import org.http4s.circe.JsonDecoder
import org.http4s.dsl.Http4sDsl
import org.http4s.server.{ AuthMiddleware, Router }
import org.http4s.{ AuthedRoutes, HttpRoutes }

final class AdminTariffRoutes[F[_]: Defer: JsonDecoder: MonadThrow](tariffs: Tariffs[F]) extends Http4sDsl[F] {

  private[routes] val pathPrefix = "/tariffs"

  private val httpRoutes: AuthedRoutes[AdminUser, F] = AuthedRoutes.of {
    case ar @ POST -> Root as _ =>
      ar.req.decodeR[TariffCreateParam] { tcp =>
        val (tariffName, hold, active) = tcp.toDomain
        Created(tariffs.create(tariffName, hold, active))
      }
  }

  def routes(authMiddleware: AuthMiddleware[F, AdminUser]): HttpRoutes[F] =
    Router(pathPrefix -> authMiddleware(httpRoutes))
}
