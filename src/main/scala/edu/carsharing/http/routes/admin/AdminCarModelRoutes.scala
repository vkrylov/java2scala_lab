package edu.carsharing.http.routes.admin

import cats.Defer
import edu.carsharing.algebras.auth.UsersAuth.AdminUser
import edu.carsharing.algebras.carmodel.CarModels
import edu.carsharing.domain.carmodel.ModelNameParam
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.decoder._
import edu.carsharing.http.json.codecs._
import org.http4s.circe.JsonDecoder
import org.http4s.dsl.Http4sDsl
import org.http4s.server.{ AuthMiddleware, Router }
import org.http4s.{ AuthedRoutes, HttpRoutes }
import io.circe.refined._

final class AdminCarModelRoutes[F[_]: Defer: JsonDecoder: MonadThrow](carModelService: CarModels[F])
    extends Http4sDsl[F] {

  private[routes] val pathPrefix = "/models"

  private val httpRoutes: AuthedRoutes[AdminUser, F] = AuthedRoutes.of {
    case ar @ POST -> Root as _ =>
      ar.req.decodeR[ModelNameParam] { mnp =>
        Created(carModelService.create(mnp.toDomain))
      }
    case ar @ DELETE -> Root as _ =>
      ar.req.decodeR[ModelNameParam] { mnp =>
        Ok(carModelService.delete(mnp.toDomain))
      }
  }

  def routes(authMiddleware: AuthMiddleware[F, AdminUser]): HttpRoutes[F] =
    Router(pathPrefix -> authMiddleware(httpRoutes))
}
