package edu.carsharing.http.routes.admin

import cats.Defer
import edu.carsharing.algebras.auth.UsersAuth.AdminUser
import edu.carsharing.algebras.cars.Cars
import edu.carsharing.domain.cars.CarParam
import edu.carsharing.effects.effects.MonadThrow
import edu.carsharing.http.decoder._
import edu.carsharing.http.json.codecs._
import org.http4s.circe.JsonDecoder
import org.http4s.dsl.Http4sDsl
import org.http4s.server.{ AuthMiddleware, Router }
import org.http4s.{ AuthedRoutes, HttpRoutes }

final class AdminCarRoutes[F[_]: Defer: JsonDecoder: MonadThrow](cars: Cars[F]) extends Http4sDsl[F] {

  private[routes] val pathPrefix = "/cars"

  private val httpRoutes: AuthedRoutes[AdminUser, F] = AuthedRoutes.of {
    case ar @ POST -> Root as _ =>
      ar.req.decodeR[CarParam] { param =>
        val (lp, mn) = param.toDomain
        Ok(cars.create(lp, mn))
      }
  }

  def routes(authMiddleware: AuthMiddleware[F, AdminUser]): HttpRoutes[F] =
    Router(pathPrefix -> authMiddleware(httpRoutes))
}
