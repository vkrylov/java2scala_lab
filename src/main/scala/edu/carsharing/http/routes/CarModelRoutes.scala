package edu.carsharing.http.routes

import cats.{ Defer, Monad }
import edu.carsharing.algebras.carmodel.CarModels
import edu.carsharing.domain.carmodel.ModelNameParam
import edu.carsharing.http.json.codecs._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

final class CarModelRoutes[F[_]: Defer: Monad](carModelService: CarModels[F]) extends Http4sDsl[F] {

  private[routes] val prefixPath = "/models"

  object ModelNameQueryParam extends OptionalQueryParamDecoderMatcher[ModelNameParam]("name")

  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root :? ModelNameQueryParam(brand) =>
      Ok(brand.fold(carModelService.findAll())(b => carModelService.findBy(b.toDomain)))
  }

  val routes: HttpRoutes[F] = Router(prefixPath -> httpRoutes)
}
