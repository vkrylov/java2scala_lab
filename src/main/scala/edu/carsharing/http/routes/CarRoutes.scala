package edu.carsharing.http.routes

import cats.{ Defer, Monad }
import edu.carsharing.algebras.cars.Cars
import edu.carsharing.http.json.codecs._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

final class CarRoutes[F[_]: Defer: Monad](cars: Cars[F]) extends Http4sDsl[F] {

  private[routes] val pathPrefix = "/cars"

  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of {
    case GET -> Root =>
      Ok(cars.findAll())
  }

  def routes: HttpRoutes[F] = Router(pathPrefix -> httpRoutes)
}
