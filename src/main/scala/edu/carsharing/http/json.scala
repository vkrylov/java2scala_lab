package edu.carsharing.http

import scala.concurrent.duration.FiniteDuration

import cats.Applicative
import cats.syntax.either._
import dev.profunktor.auth.jwt.JwtToken
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.User
import edu.carsharing.algebras.healthcheck.HealthCheck.AppStatus
import edu.carsharing.domain.cars._
import edu.carsharing.domain.carmodel.CarModel
import edu.carsharing.http.client.PaymentClient.Payment
import edu.carsharing.domain.checkout.Card
import edu.carsharing.domain.rents._
import edu.carsharing.domain.tariffs.{ Tariff, TariffCreateParam }
import eu.timepit.refined.api.{ Refined, Validate }
import eu.timepit.refined.collection.Size
import eu.timepit.refined.refineV
import io.circe._
import io.circe.generic.semiauto._
import io.circe.syntax._
import io.estatico.newtype.Coercible
import io.estatico.newtype.ops._
import org.http4s.circe.jsonEncoderOf
import org.http4s.{ ParseFailure, QueryParamDecoder, _ }
import squants.market.{ Money, USD }
import concurrent.duration._
import io.circe.refined._

object json {

  object codecs extends JsonCodecs {
    implicit def deriveEntityEncoder[F[_]: Applicative, A: Encoder]: EntityEncoder[F, A] = jsonEncoderOf[F, A]
  }

}

trait JsonCodecs {

  implicit def coercibleQueryParamDecoder[A: Coercible[B, *], B: QueryParamDecoder]: QueryParamDecoder[A] =
    QueryParamDecoder[B].map(_.coerce[A])

  implicit def refinedQueryParamDecoder[T: QueryParamDecoder, P](
    implicit ev: Validate[T, P]
  ): QueryParamDecoder[T Refined P] =
    QueryParamDecoder[T].emap(refineV[P](_).leftMap(m => ParseFailure(m, m)))

  implicit def coercibleDecoder[A: Coercible[B, *], B: Decoder]: Decoder[A] =
    Decoder[B].map(_.coerce[A])

  implicit def coercibleEncoder[A: Coercible[B, *], B: Encoder]: Encoder[A] =
    Encoder[B].contramap(_.asInstanceOf[B])

  implicit def coercibleKeyDecoder[A: Coercible[B, *], B: KeyDecoder]: KeyDecoder[A] =
    KeyDecoder[B].map(_.coerce[A])

  implicit def coercibleKeyEncoder[A: Coercible[B, *], B: KeyEncoder]: KeyEncoder[A] =
    KeyEncoder[B].contramap[A](_.asInstanceOf[B])

  implicit val moneyDecoder: Decoder[Money] =
    Decoder[BigDecimal].map(USD.apply)

  implicit val moneyEncoder: Encoder[Money] =
    Encoder[BigDecimal].contramap(_.amount)

  implicit val carDecoder: Decoder[Car] = deriveDecoder
  implicit val carEncoder: Encoder[Car] = deriveEncoder

  implicit val carModelDecoder: Decoder[CarModel] = deriveDecoder
  implicit val carModelEncoder: Encoder[CarModel] = deriveEncoder

  implicit val tariffDecoder: Decoder[Tariff] = deriveDecoder
  implicit val tariffEncoder: Encoder[Tariff] = deriveEncoder

  implicit val durationEncoder: Encoder[FiniteDuration] = Encoder(_.toMillis.asJson)
  implicit val durationDecoder: Decoder[FiniteDuration] = Decoder(_.as[Long].map(_.millis))

  implicit val rentEventDecoder: Decoder[RentEvent] = deriveDecoder
  implicit val rentEventEncoder: Encoder[RentEvent] = deriveEncoder

  implicit val rentEventsDecoder: Decoder[RentSummary] = deriveDecoder
  implicit val rentEventsEncoder: Encoder[RentSummary] = deriveEncoder

  implicit val rentEventListDecoder: Decoder[List[RentEvent]] = deriveDecoder
  implicit val rentEventListEncoder: Encoder[List[RentEvent]] = deriveEncoder

  implicit val rentEventKeyDecoder: KeyDecoder[RentEvent] = (key: String) =>
    io.circe.parser.decode[RentEvent](key).toOption
  implicit val rentEventKeyEncoder: KeyEncoder[RentEvent] = (key: RentEvent) => key.asJson.noSpaces

  implicit val tariffKeyDecoder: KeyDecoder[Tariff] = (key: String) => io.circe.parser.decode[Tariff](key).toOption
  implicit val tariffKeyEncoder: KeyEncoder[Tariff] = (key: Tariff) => key.asJson.noSpaces

  implicit val rentTypeChangeParamDecoder: Decoder[RentTypeChangeParam] = Decoder(
    _.as[String].map(RentTypeChangeParam.apply)
  )
  implicit val rentTypeChangeParamEncoder: Encoder[RentTypeChangeParam] = Encoder(_.toString.toLowerCase.asJson)

  implicit def validateSizeN[N <: Int, R](implicit w: ValueOf[N]): Validate.Plain[R, Size[N]] =
    Validate
      .fromPredicate[R, Size[N]](_.toString.size == w.value, _ => s"Must have ${w.value} digits", Size[N](w.value))

  implicit val cardDecoder: Decoder[Card] = deriveDecoder
  implicit val cardEncoder: Encoder[Card] = deriveEncoder

  implicit val loginUserDecoder: Decoder[LoginUser] = deriveDecoder
  implicit val loginUserEncoder: Encoder[LoginUser] = deriveEncoder

  implicit val tokenEncoder: Encoder[JwtToken] =
    Encoder.forProduct1("access_token")(_.value)

  implicit val createUserDecoder: Decoder[CreateUser] = deriveDecoder

  implicit val paymentDecoder: Encoder[Payment] = deriveEncoder
  implicit val paymentEncoder: Decoder[Payment] = deriveDecoder

  implicit val userDecoder: Decoder[User] = deriveDecoder
  implicit val userEncoder: Encoder[User] = deriveEncoder

  implicit val appStatusDecoder: Decoder[AppStatus] = deriveDecoder
  implicit val appStatusEncoder: Encoder[AppStatus] = deriveEncoder

  implicit val createRentParamsDecoder: Decoder[CreateRentParams] = deriveDecoder
  implicit val createRentParamsEncoder: Encoder[CreateRentParams] = deriveEncoder

  implicit val tariffCreateParamDecoder: Decoder[TariffCreateParam] = deriveDecoder
  implicit val tariffCreateParamEncoder: Encoder[TariffCreateParam] = deriveEncoder

  implicit val carParamDecoder: Decoder[CarParam] = deriveDecoder
}
