package edu.carsharing.redis

import cats.Monad
import cats.effect.{ Concurrent, ContextShift, Resource }
import dev.profunktor.redis4cats.{ Redis, RedisCommands }
import dev.profunktor.redis4cats.connection.{ RedisClient, RedisURI }
import dev.profunktor.redis4cats.data.RedisCodec
import dev.profunktor.redis4cats.effect.Log
import edu.carsharing.effects.effects.ApThrow

object redis {

  def mkRedisResource[F[_]: Monad: ApThrow: Log: ContextShift: Concurrent]
    : Resource[F, RedisCommands[F, String, String]] =
    for {
      uri <- Resource.liftF(RedisURI.make[F]("redis://localhost"))
      cli <- RedisClient[F].fromUri(uri)
      cmd <- Redis[F].fromClient(cli, RedisCodec.Utf8)
    } yield cmd
}
