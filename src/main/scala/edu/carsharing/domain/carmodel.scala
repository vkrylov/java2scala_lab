package edu.carsharing.domain

import java.util.UUID

import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.macros.newtype

object carmodel {
  @newtype case class ModelName(name: String)

  @newtype case class ModelId(id: UUID)
  case class CarModel(id: ModelId, name: ModelName)

  @newtype case class ModelNameParam(name: NonEmptyString) {
    def toDomain: ModelName = ModelName(name.value)
  }
}
