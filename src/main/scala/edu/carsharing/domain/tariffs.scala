package edu.carsharing.domain

import java.util.UUID

import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.macros.newtype
import squants.Money
import squants.market.RUB

object tariffs {
  @newtype case class TariffId(uuid: UUID)
  @newtype case class TariffName(name: String)

  case class Tariff(tariffId: TariffId, name: TariffName, hold: Money, active: Money)
  case class TariffCreateParam(name: NonEmptyString, hold: Double, active: Double) {
    def toDomain: (TariffName, Money, Money) = (TariffName(name.value), RUB(hold), RUB(active))
  }
}
