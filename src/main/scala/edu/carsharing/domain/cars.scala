package edu.carsharing.domain

import java.util.UUID

import scala.util.control.NoStackTrace

import edu.carsharing.domain.carmodel.ModelName
import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.macros.newtype

object cars {
  @newtype case class LicensePlate(number: String)
  @newtype case class CarId(uuid: UUID)
  case class Car(id: CarId, licensePlate: LicensePlate, modelName: ModelName)

  case class CarParam(licensePlate: NonEmptyString, model: NonEmptyString) {
    def toDomain: (LicensePlate, ModelName) = (LicensePlate(licensePlate.value), ModelName(model.value))
  }

  case object AlreadyStarted extends Exception with NoStackTrace
  case object NotStarted extends Exception with NoStackTrace
}
