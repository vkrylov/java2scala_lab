package edu.carsharing.domain

import java.util.UUID

import scala.concurrent.duration.FiniteDuration
import scala.util.control.NoStackTrace

import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.tariffs.TariffName
import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.macros.newtype
import squants.Money

object rents {
  @newtype case class RentId(uuid: UUID)

  val ActiveEvent   = "active"
  val HoldEvent     = "hold"
  val CompleteEvent = "complete"

  case class RentEvent(timestamp: FiniteDuration, eventType: String)

  case class RentSummary(
    events: List[RentEvent],
    total: Money,
    licensePlate: LicensePlate,
    rentId: RentId,
    tariffName: TariffName
  )

  case class RentNotFound(userId: UserId) extends NoStackTrace

  @newtype case class RentTypeChange(rentType: String)

  sealed trait RentTypeChangeParam

  object RentTypeChangeParam {
    case object ActivateParam extends RentTypeChangeParam
    case object OnHoldParam extends RentTypeChangeParam

    private val ap = ActivateParam.toString.toLowerCase
    private val hp = OnHoldParam.toString.toLowerCase

    def apply(str: String): RentTypeChangeParam = str.toLowerCase match {
      case `ap` => ActivateParam
      case `hp` => OnHoldParam
    }
  }

  case class CreateRentParams(tariffName: NonEmptyString, licensePlate: NonEmptyString) {
    def toDomain: (TariffName, LicensePlate) = TariffName(tariffName.value) -> LicensePlate(licensePlate.value)
  }
}
