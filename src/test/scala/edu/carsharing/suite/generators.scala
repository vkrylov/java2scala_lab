package edu.carsharing.suite

import java.util.UUID

import scala.concurrent.duration._

import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.domain.carmodel._
import edu.carsharing.domain.cars._
import edu.carsharing.domain.checkout._
import edu.carsharing.domain.rents._
import edu.carsharing.domain.tariffs._
import eu.timepit.refined.api.Refined
import io.estatico.newtype.Coercible
import io.estatico.newtype.ops._
import org.scalacheck.{ Arbitrary, Gen }
import squants.Money
import squants.market.RUB

object generators {

  def cbUuid[A: Coercible[UUID, *]]: Gen[A] =
    Gen.uuid.map(_.coerce[A])

  implicit def arbCoercibleUUID[A: Coercible[UUID, *]]: Arbitrary[A] =
    Arbitrary(cbUuid[A])

  def cbInt[A: Coercible[Int, *]]: Gen[A] =
    Gen.posNum[Int].map(_.coerce[A])

  implicit def arbCoercibleInt[A: Coercible[Int, *]]: Arbitrary[A] =
    Arbitrary(cbInt[A])

  val genNonEmptyString: Gen[String] =
    Gen
      .chooseNum(10, 30)
      .flatMap { n =>
        Gen.buildableOfN[String, Char](n, Gen.alphaChar)
      }

  def cbStr[A: Coercible[String, *]]: Gen[A] =
    genNonEmptyString.map(_.coerce[A])

  val genMoney: Gen[Money] =
    Gen.posNum[Long].map(n => RUB(BigDecimal(n)))

  implicit val arbMoney: Arbitrary[Money] = Arbitrary(genMoney)

  val tariffNameGen: Gen[TariffName] = cbStr[TariffName]

  implicit val arbTariffName: Arbitrary[TariffName] = Arbitrary(tariffNameGen)

  val tariffGen: Gen[Tariff] =
    for {
      t <- cbUuid[TariffId]
      n <- tariffNameGen
      h <- genMoney
      a <- genMoney
    } yield Tariff(t, n, h, a)

  val rentItemGen: Gen[RentEvent] =
    for {
      t <- Gen.posNum[Int]
      e <- genNonEmptyString
    } yield RentEvent(t.seconds, e)

  val licensePlateGen: Gen[LicensePlate] = cbStr[LicensePlate]

  implicit val arbLicensePlate: Arbitrary[LicensePlate] = Arbitrary(licensePlateGen)

  val cartTotalGen: Gen[RentSummary] =
    for {
      i <- Gen.nonEmptyListOf(rentItemGen)
      t <- genMoney
      lp <- licensePlateGen
      r <- cbUuid[RentId]
      tn <- tariffNameGen
    } yield RentSummary(i, t, lp, r, tn)

  implicit val arbRentTotal: Arbitrary[RentSummary] =
    Arbitrary(cartTotalGen)

  val cardGen: Gen[Card] =
    for {
      n <- genNonEmptyString.map[CardNamePred](Refined.unsafeApply)
      u <- Gen.posNum[Long].map[CardNumberPred](Refined.unsafeApply)
      x <- Gen
            .posNum[Int]
            .map[CardExpirationPred](x => Refined.unsafeApply(x.toString))
      c <- Gen.posNum[Int].map[CardCVVPred](Refined.unsafeApply)
    } yield Card(CardName(n), CardNumber(u), CardExpiration(x), CardCVV(c))

  implicit val arbCard: Arbitrary[Card] =
    Arbitrary(cardGen)

  val carIdGen: Gen[CarId] = Gen.uuid.map(_.coerce[CarId])

  val modelNameGen: Gen[ModelName] = cbStr[ModelName]

  implicit val arbModelName: Arbitrary[ModelName] = Arbitrary(modelNameGen)

  val carModelGen: Gen[CarModel] = for {
    id <- cbUuid[ModelId]
    n <- modelNameGen
  } yield CarModel(id, n)

  implicit val arbCarModel: Arbitrary[CarModel] = Arbitrary(carModelGen)

  val userNameGen: Gen[UserName] = cbStr[UserName]

  implicit val arbUserName: Arbitrary[UserName] = Arbitrary(userNameGen)

  val passwordGen: Gen[Password] = cbStr[Password]

  implicit val arbPassword: Arbitrary[Password] = Arbitrary(passwordGen)

  val userPasswordGen: Gen[(UserName, Password)] =
    for {
      un <- userNameGen
      p <- passwordGen
    } yield (un, p)

  implicit val arbUserPassword: Arbitrary[(UserName, Password)] = Arbitrary(userPasswordGen)

  val itemListGen: Gen[List[RentEvent]] = Gen.nonEmptyListOf(rentItemGen)

  implicit val arbRentItemList: Arbitrary[List[RentEvent]] = Arbitrary(itemListGen)
}
