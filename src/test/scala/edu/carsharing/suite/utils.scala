package edu.carsharing.suite

import scala.concurrent.duration.FiniteDuration

import cats.effect.IO
import cats.effect.concurrent.Ref
import edu.carsharing.effects.Background
import io.chrisdavenport.log4cats.Logger

object utils {

  object IOAssertion {
    def apply[A](ioa: IO[A]): Unit = ioa.void.unsafeRunSync()
  }

  object background {
    val NoOp: Background[IO] =
      new Background[IO] {
        def schedule[A](fa: IO[A], duration: FiniteDuration): IO[Unit] = IO.unit
      }

    def counter(ref: Ref[IO, Int]): Background[IO] =
      new Background[IO] {
        def schedule[A](fa: IO[A], duration: FiniteDuration): IO[Unit] = ref.update(_ + 1)
      }
  }
  object logger {
    implicit object NoOp extends NoLogger

    def acc(ref: Ref[IO, List[String]]): Logger[IO] =
      new NoLogger {
        override def error(message: => String): IO[Unit] =
          ref.update(xs => message :: xs)
      }
  }
}
