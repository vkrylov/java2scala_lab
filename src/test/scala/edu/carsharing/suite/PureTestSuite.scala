package edu.carsharing.suite

import scala.concurrent.ExecutionContext

import cats.effect._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

trait PureTestSuite extends AnyFunSuite with ScalaCheckDrivenPropertyChecks with CatsEquality {

  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)
  implicit val timer: Timer[IO]     = IO.timer(ExecutionContext.global)

}
