package edu.carsharing.suite

import cats.effect.IO
import io.chrisdavenport.log4cats.Logger

class NoLogger extends Logger[IO] {
  def warn(message: => String): IO[Unit]                = IO.unit
  def warn(t: Throwable)(message: => String): IO[Unit]  = IO.unit
  def debug(t: Throwable)(message: => String): IO[Unit] = IO.unit
  def debug(message: => String): IO[Unit]               = IO.unit
  def error(t: Throwable)(message: => String): IO[Unit] = IO.unit
  def error(message: => String): IO[Unit]               = IO.unit
  def info(t: Throwable)(message: => String): IO[Unit]  = IO.unit
  def info(message: => String): IO[Unit]                = IO.unit
  def trace(t: Throwable)(message: => String): IO[Unit] = IO.unit
  def trace(message: => String): IO[Unit]               = IO.unit
}
