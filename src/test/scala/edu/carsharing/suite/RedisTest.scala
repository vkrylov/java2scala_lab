package edu.carsharing.suite

import java.util.UUID

import cats.effect.{ IO, Resource }
import ciris.Secret
import dev.profunktor.auth.jwt._
import dev.profunktor.redis4cats.RedisCommands
import dev.profunktor.redis4cats.connection.{ RedisClient, RedisURI }
import dev.profunktor.redis4cats.data.RedisCodec
import dev.profunktor.redis4cats.Redis
import dev.profunktor.redis4cats.log4cats._
import edu.carsharing.algebras.tokens.Tokens._
import edu.carsharing.suite.utils.{ logger, IOAssertion }
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth.User
import edu.carsharing.algebras.auth._
import edu.carsharing.algebras.tokens.LiveTokens
import edu.carsharing.algebras.users.Users
import edu.carsharing.effects.GenUUID
import eu.timepit.refined.types.string.NonEmptyString
import io.chrisdavenport.log4cats.Logger
import pdi.jwt.{ JwtAlgorithm, JwtClaim }

class RedisTest extends ResourceSuite[RedisCommands[IO, String, String]] {

  // For it:tests, one test is enough
  val MaxTests: PropertyCheckConfigParam = MinSuccessful(1)

  implicit val log: Logger[IO] = logger.NoOp

  override def resources =
    for {
      uri <- Resource.liftF(RedisURI.make[IO]("redis://localhost"))
      client <- RedisClient[IO].fromUri(uri)
      cmd <- Redis[IO].fromClient(client, RedisCodec.Utf8)
    } yield cmd

  import concurrent.duration._

  import cats.Eq
  import cats.instances.option._
  import cats.syntax.alternative._
  import cats.syntax.applicative._
  import cats.syntax.functor._
  import eu.timepit.refined.auto._
  import eu.timepit.refined.cats._
  import edu.carsharing.common.Domain._

  class TestUsers(un: UserName) extends Users[IO] {
    def find(username: UserName, password: Password): IO[Option[User]] =
      (Eq[UserName]
        .eqv(username, un))
        .guard[Option]
        .as(User(UserId(UUID.randomUUID), un))
        .pure[IO]
    def create(username: UserName, password: Password): IO[UserId] =
      GenUUID[IO].make[UserId]
  }

  import cats.instances.string._
  import generators._

  lazy val tokenConfig = JwtSecretKeyConfig(Secret("bar": NonEmptyString))
  lazy val tokenExp    = TokenExpiration(30.seconds)
  lazy val jwtClaim    = JwtClaim("test")
  lazy val userJwtAuth = UserJwtAuth(JwtAuth.hmac("bar", JwtAlgorithm.HS256))

  withResources { cmd =>
    test("Authentication") {
      forAll(MaxTests) { (un1: UserName, un2: UserName, pw: Password) =>
        IOAssertion {
          for {
            t <- LiveTokens.make[IO](tokenConfig, tokenExp)
            a <- LiveAuth.make[IO](tokenExp, t, new TestUsers(un2), cmd)
            u <- LiveUsersAuth.make[IO](cmd)
            x <- u.findUser(JwtToken("invalid"))(jwtClaim)
            j <- a.newUser(un1, pw)
            e <- jwtDecode[IO](j, userJwtAuth.value).attempt
            k <- a.login(un2, pw)
            f <- jwtDecode[IO](k, userJwtAuth.value).attempt
            _ <- a.logout(k, un2)
            y <- u.findUser(k)(jwtClaim)
            w <- u.findUser(j)(jwtClaim)
          } yield assert(
            x.isEmpty && e.isRight && f.isRight && y.isEmpty &&
              w.fold(false)(_.value.name === un1)
          )
        }
      }
    }
  }
}
