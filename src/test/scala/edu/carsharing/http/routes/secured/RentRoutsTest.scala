package edu.carsharing.http.routes.secured

import java.util.UUID

import cats.data.Kleisli
import cats.effect.IO
import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.auth.UsersAuth._
import edu.carsharing.algebras.rents.Rents
import edu.carsharing.domain.cars.LicensePlate
import edu.carsharing.domain.rents.{ RentId, RentSummary }
import edu.carsharing.domain.tariffs.TariffName
import edu.carsharing.suite.HttpTestSuite
import edu.carsharing.suite.utils.IOAssertion
import org.http4s.Method._
import org.http4s.client.dsl.io._
import org.http4s.server.AuthMiddleware
import org.http4s.{ Status, Uri }
import squants.market.RUB

class RentRoutsTest extends HttpTestSuite {

  import edu.carsharing.http.json.codecs._
  import edu.carsharing.suite.generators._

  val authUser =
    CommonUser(User(UserId(UUID.randomUUID), UserName("user")))

  val authMiddleware: AuthMiddleware[IO, CommonUser] =
    AuthMiddleware(Kleisli.pure(authUser))

  def rentTotalSuccess(rentSummary: RentSummary): Rents[IO] =
    new TestRents {
      override def total(userId: UserId): IO[RentSummary] = IO.pure(rentSummary)
    }

  test("GET rent [OK]") {
    forAll { (rt: RentSummary) =>
      IOAssertion {
        GET(Uri.uri("/rent")).flatMap { req =>
          val routes = new RentRoutes[IO](rentTotalSuccess(rt)).routes(authMiddleware)
          assertHttp(routes, req)(Status.Ok, rt)
        }
      }
    }
  }

}

class TestRents extends Rents[IO] {

  override def create(userId: UserId, tariffName: TariffName, licensePlate: LicensePlate): IO[Unit] = IO.unit

  override def onHold(userId: UserId): IO[Unit] = IO.unit

  override def activate(userId: UserId): IO[Unit] = IO.unit

  override def total(userId: UserId): IO[RentSummary] =
    IO.pure(RentSummary(List.empty, RUB(0), LicensePlate("lp"), RentId(UUID.randomUUID()), TariffName("tn")))

  override def delete(userId: UserId): IO[Unit] = IO.unit
}
