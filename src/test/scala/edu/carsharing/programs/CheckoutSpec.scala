package edu.carsharing.programs

import cats.effect.IO
import cats.effect.concurrent.Ref
import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.algebras.rents.Rents
import edu.carsharing.domain.checkout.Card
import edu.carsharing.domain.rents.{ RentEvent, RentSummary }
import edu.carsharing.domain.{ cars, rents, tariffs }
import edu.carsharing.effects.Background
import edu.carsharing.http.client.PaymentClient
import edu.carsharing.http.client.PaymentClient.Payment
import edu.carsharing.suite.PureTestSuite
import edu.carsharing.suite.utils.{ background, logger, IOAssertion }
import io.chrisdavenport.log4cats.Logger
import retry.{ RetryPolicies, RetryPolicy }
import squants.Money

class CheckoutSpec extends PureTestSuite {

  def successfulClient(pid: PaymentId): PaymentClient[IO] =
    (_: Payment) => IO.pure(pid)

  def successfulRent(rent: RentSummary): Rents[IO] =
    new TestRents[IO] {
      override def total(userId: UserId): IO[RentSummary] =
        IO.pure(rent)
      override def delete(userId: UserId): IO[Unit] =
        IO.unit
    }

  def successfulOrders(oid: OrderId): Orders[IO] =
    new TestOrders[IO] {

      override def create(
        userId: UserId,
        paymentId: PaymentId,
        licensePlate: cars.LicensePlate,
        tariffName: tariffs.TariffName,
        rentId: rents.RentId,
        events: List[RentEvent],
        total: Money
      ): IO[OrderId] =
        IO.pure(oid)
    }

  val MaxRetries                   = 3
  val retryPolicy: RetryPolicy[IO] = RetryPolicies.limitRetries(MaxRetries)

  import edu.carsharing.suite.generators._

  test(s"successful checkout") {
    implicit val bg: Background[IO] = background.NoOp
    import edu.carsharing.suite.utils.logger.NoOp
    forAll { (uid: UserId, pid: PaymentId, oid: OrderId, ct: RentSummary, card: Card) =>
      IOAssertion {
        new CheckoutProgram[IO](successfulClient(pid), successfulRent(ct), successfulOrders(oid), retryPolicy)
          .checkout(uid, card)
          .map { id =>
            assert(id == oid)
          }
      }
    }
  }

  val unreachableClient: PaymentClient[IO] =
    new PaymentClient[IO] {
      def process(payment: Payment): IO[PaymentId] =
        IO.raiseError(PaymentError(""))
    }

  test("unreachable client") {
    forAll { (uid: UserId, rt: RentSummary, oid: OrderId, card: Card) =>
      implicit val bg: Background[IO] = background.NoOp
      IOAssertion {
        Ref.of[IO, List[String]](List.empty).flatMap { logs =>
          implicit val log: Logger[IO] = logger.acc(logs)
          new CheckoutProgram[IO](unreachableClient, successfulRent(rt), successfulOrders(oid), retryPolicy)
            .checkout(uid, card)
            .attempt
            .flatMap {
              case Left(PaymentError(_)) =>
                logs.get.map {
                  case (x :: xs) =>
                    assert(
                      x.contains("Giving up") &&
                        xs.size == MaxRetries
                    )
                  case _ =>
                    fail(s"Expected $MaxRetries retries")
                }
              case e => fail(s"Expected payment error $e")
            }
        }
      }
    }
  }

  def recoveringClient(attemptsSoFar: Ref[IO, Int], paymentId: PaymentId): PaymentClient[IO] =
    new PaymentClient[IO] {
      def process(payment: Payment): IO[PaymentId] =
        attemptsSoFar.get.flatMap {
          case n if n == 1 =>
            IO.pure(paymentId)
          case _ =>
            attemptsSoFar.update(_ + 1) *>
                IO.raiseError(PaymentError(""))
        }
    }

  test("recovering client") {
    forAll { (uid: UserId, pid: PaymentId, rt: RentSummary, oid: OrderId, card: Card) =>
      IOAssertion {
        Ref.of[IO, List[String]](List.empty).flatMap { logs =>
          Ref.of[IO, Int](1 - 1).flatMap { attemptsSoFar =>
            implicit val bg: Background[IO] = background.NoOp
            implicit val log: Logger[IO]    = logger.acc(logs)
            new CheckoutProgram[IO](
              recoveringClient(attemptsSoFar, pid),
              successfulRent(rt),
              successfulOrders(oid),
              retryPolicy
            ).checkout(uid, card)
              .attempt
              .flatMap {
                case Right(id) =>
                  logs.get.map { xs =>
                    assert(id == oid && xs.size == 1)
                  }
                case Left(_) => fail("Expected Payment Id")
              }
          }
        }
      }
    }
  }

  val failingOrders: Orders[IO] =
    new TestOrders[IO] {
      override def create(
        userId: UserId,
        paymentId: PaymentId,
        licensePlate: cars.LicensePlate,
        tariffName: tariffs.TariffName,
        rentId: rents.RentId,
        events: List[RentEvent],
        total: Money
      ): IO[OrderId] =
        IO.raiseError(OrderError(""))
    }

  import cats.syntax.apply._

  test("failing orders") {
    forAll { (uid: UserId, pid: PaymentId, rt: RentSummary, card: Card) =>
      IOAssertion {
        Ref.of[IO, Int](0).flatMap { counterRef =>
          Ref.of[IO, List[String]](List.empty).flatMap { logs =>
            implicit val bg: Background[IO] = background.counter(counterRef)
            implicit val log: Logger[IO]    = logger.acc(logs)
            new CheckoutProgram[IO](successfulClient(pid), successfulRent(rt), failingOrders, retryPolicy)
              .checkout(uid, card)
              .attempt
              .flatMap {
                case Left(OrderError(_)) =>
                  (counterRef.get, logs.get).mapN {
                    case (c, (x :: y :: xs)) =>
                      assert(
                        x.contains("Rescheduling") && y
                            .contains("Giving up") && xs.size == MaxRetries && c == 1
                      )
                    case _ =>
                      fail(s"Expected $MaxRetries retries and reschedule")
                  }
                case _ =>
                  fail("Expected order error")
              }
          }
        }
      }
    }
  }

  def failingCart(cartTotal: RentSummary): Rents[IO] =
    new TestRents[IO] {
      override def total(userId: UserId): IO[RentSummary] =
        IO.pure(cartTotal)
      override def delete(userId: UserId): IO[Unit] =
        IO.raiseError(new Exception(""))
    }

  test("failing to delete cart does not affect checkout") {
    forAll { (uid: UserId, pid: PaymentId, rt: RentSummary, oid: OrderId, card: Card) =>
      IOAssertion {
        implicit val bg: Background[IO] = background.NoOp
        import logger.NoOp
        new CheckoutProgram[IO](successfulClient(pid), failingCart(rt), successfulOrders(oid), retryPolicy)
          .checkout(uid, card)
          .map { id =>
            assert(id == oid)
          }
      }
    }
  }

}
