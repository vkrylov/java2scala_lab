package edu.carsharing.programs

import edu.carsharing.algebras.auth.Auth.UserId
import edu.carsharing.algebras.orders.Orders
import edu.carsharing.algebras.orders.Orders._
import edu.carsharing.domain.rents.RentEvent
import edu.carsharing.domain.{ cars, rents, tariffs }
import squants.Money

class TestOrders[F[_]] extends Orders[F] {
  override def get(userId: UserId, orderId: OrderId): F[Option[Order]] = ???

  override def findBy(userId: UserId): F[List[Order]] = ???

  override def create(
    userId: UserId,
    paymentId: PaymentId,
    licensePlate: cars.LicensePlate,
    tariffName: tariffs.TariffName,
    rentId: rents.RentId,
    events: List[RentEvent],
    total: Money
  ): F[OrderId] = ???
}
