package edu.carsharing.programs

import edu.carsharing.algebras.auth.Auth._
import edu.carsharing.algebras.rents.Rents
import edu.carsharing.domain.rents.RentSummary
import edu.carsharing.domain.{ cars, tariffs }

class TestRents[F[_]] extends Rents[F] {

  override def create(userId: UserId, tariffName: tariffs.TariffName, licensePlate: cars.LicensePlate): F[Unit] = ???

  override def onHold(userId: UserId): F[Unit] = ???

  override def activate(userId: UserId): F[Unit] = ???

  override def total(userId: UserId): F[RentSummary] = ???

  override def delete(userId: UserId): F[Unit] = ???
}
